package com.galaxy.carlaodelivery.list.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.model.Pedido;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.util.Convert;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PedidoAdapter extends BaseAdapter {
    private final List<Pedido> listPedidos;
    private final Activity activity;

    public PedidoAdapter(List<Pedido> listPedidos, Activity activity){
        this.listPedidos = listPedidos;
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return listPedidos.size();
    }

    @Override
    public Object getItem(int i) {
        return listPedidos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return listPedidos.get(i).getCodigo();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.pedido_list_row, parent, false);
        Pedido pedido = listPedidos.get(i);

        TextView valorCodigoPedidoTxtVw = (TextView) view.findViewById(R.id.valorCodigoPedidoTxtVwPedListRow);
        TextView dataPedidoTxtVw = (TextView) view.findViewById(R.id.dataPedidoTxtVwPedListRow);
        TextView statusPedidoTxtVw = (TextView) view.findViewById(R.id.statusPedidoTxtVwPedListRow);

        valorCodigoPedidoTxtVw.setText(Convert.toString(pedido.getCodigo()));
        dataPedidoTxtVw.setText(UtilsApp.getDateWithLocale(pedido.getCriadoEm(), Parameters.STANDARD_DATE_FORMAT_SIMPLE));
        statusPedidoTxtVw.setText(pedido.getStatusPedido().getNome());

        return view;

    }
}
