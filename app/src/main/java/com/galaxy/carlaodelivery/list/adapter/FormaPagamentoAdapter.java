package com.galaxy.carlaodelivery.list.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.galaxy.carlaodelivery.model.FormaPagamento;

import java.util.List;

public class FormaPagamentoAdapter extends ArrayAdapter<FormaPagamento>{

    private Context context;
    private List<FormaPagamento> values;

    public FormaPagamentoAdapter(Context context, int textViewResourceId, List<FormaPagamento> values){
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public FormaPagamento getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getNome());

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getNome());

        return label;
    }


}
