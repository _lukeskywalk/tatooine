package com.galaxy.carlaodelivery.list.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.model.PedidoProduto;
import com.galaxy.korriban.util.Convert;

import org.w3c.dom.Text;

import java.util.List;

public class PedidoProdutoAdapter extends BaseAdapter{
    private final List<PedidoProduto> listPedidoProduto;
    private final Activity activity;


    public PedidoProdutoAdapter(List<PedidoProduto> listPedidoProduto, Activity activity){
        this.listPedidoProduto = listPedidoProduto;
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return listPedidoProduto.size();
    }

    @Override
    public Object getItem(int i) {
        return listPedidoProduto.get(i);
    }

    @Override
    public long getItemId(int i) {
        return listPedidoProduto.get(i).getCodigo();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.pedido_produto_list_row, parent, false);
        PedidoProduto pedidoProduto = listPedidoProduto.get(i);

        TextView nomeProdutoTxtVw = (TextView) view.findViewById(R.id.nomeProdutoTxtVwPedidoProdutoListRow);
        TextView quantidadeProdutoTxtVw = (TextView) view.findViewById(R.id.quantidadeProdutotxtVwPedidoProdutoListRow);

        nomeProdutoTxtVw.setText(pedidoProduto.getProduto().getNome());
        quantidadeProdutoTxtVw.setText(Convert.toString(pedidoProduto.getQuantidade()));

        return view;
    }
}
