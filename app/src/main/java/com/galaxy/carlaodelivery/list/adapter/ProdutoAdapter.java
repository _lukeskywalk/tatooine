package com.galaxy.carlaodelivery.list.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.model.Produto;

import java.util.List;

public class ProdutoAdapter extends BaseAdapter{

    private final List<Produto> produtos;
    private final Activity activity;

    public ProdutoAdapter(List<Produto> produtos, Activity activity){
        this.produtos = produtos;
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return produtos.size();
    }

    @Override
    public Object getItem(int i) {
        return produtos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = activity.getLayoutInflater().inflate(R.layout.product_list_row, viewGroup, false);
        Produto produto = produtos.get(i);

        TextView codigoProdutoTxtVw = (TextView) view.findViewById(R.id.codigoProdutoTxtVwListRow);
        TextView nomeProdutoTxtVw = (TextView) view.findViewById(R.id.nomeProdutoTxtVwListRow);

        codigoProdutoTxtVw.setText(String.valueOf(produto.getCodigo()));
        nomeProdutoTxtVw.setText(produto.getNome());

        return view;
    }
}
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.widget.ArrayAdapter;
//import android.widget.TextView;
//
//
//import com.galaxy.carlaodelivery.R;
//import com.galaxy.carlaodelivery.model.Produto;
//
//import java.util.LinkedList;
//import java.util.List;
//
//public class ProdutoAdapter extends ArrayAdapter<Produto> implements View.OnClickListener{
//
//    private List<Produto> listProdutos;
//    Context context;
//    private int lasPosition = -1;
//
//    @Override
//    public void onClick(View view) {
//
//        view.getId();
//
//
//        System.out.println("tocado " + view.getId());
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent){
//        Produto produto = getItem(position);
//
//        ViewHolder viewHolder;
//
//        final View result;
//
//        if(convertView == null){
//            viewHolder = new ViewHolder();
//            LayoutInflater inflater = LayoutInflater.from(getContext());
//            convertView = inflater.inflate(R.layout.product_list_row, parent, false);
//            viewHolder.codigoProdutoTxtVw = (TextView) convertView.findViewById(R.id.codigoProdutoTxtVwListRow);
//            viewHolder.nomeProdutoTxtVw = (TextView) convertView.findViewById(R.id.nomeProdutoTxtVwListRow);
//
//            result = convertView;
//            convertView.setTag(viewHolder);
//        }else{
//            viewHolder = (ViewHolder) convertView.getTag();
//            result = convertView;
//        }
//
//        Animation animation = AnimationUtils.loadAnimation(context, (position > lasPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
//        result.startAnimation(animation);
//        lasPosition = position;
//
//        viewHolder.codigoProdutoTxtVw.setText("TESTE");
//        viewHolder.nomeProdutoTxtVw.setText("AQUI");
//        convertView.setOnClickListener(this);
//
//        return convertView;
//
//    }
//
//    private static class ViewHolder{
//        TextView codigoProdutoTxtVw;
//        TextView nomeProdutoTxtVw;
//    }
//
//    public ProdutoAdapter(List<Produto> listProdutos, Context context){
//        super(context, R.layout.product_list_row, listProdutos);
//        this.listProdutos = listProdutos;
//        this.context = context;
//    }
//}
