package com.galaxy.carlaodelivery.list.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.model.Produto;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.util.Convert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class CarrinhoComprasAdapter extends BaseAdapter{
    private final List<Map<String, Object>> listProdutosCarrinhoCompras;
    private final Activity activity;

    public CarrinhoComprasAdapter(List<Map<String, Object>> listProdutosCarrinhoCompras, Activity activity){
        this.listProdutosCarrinhoCompras = listProdutosCarrinhoCompras;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return listProdutosCarrinhoCompras.size();
    }

    @Override
    public Object getItem(int i) {
        return listProdutosCarrinhoCompras.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.carrinho_compras_list_row, parent, false);
        Map<String, Object> produtoCarrinhoCompras = listProdutosCarrinhoCompras.get(position);

        TextView nomeProdutoCarCom = (TextView) view.findViewById(R.id.nomeProdutoCarComTxtVw);
        TextView quantidadeProdutoCarCom = (TextView) view.findViewById(R.id.quantidadeProdutoCarComTxtVw);
        TextView subTotalProdutoCarCom = (TextView) view.findViewById(R.id.subTotalProdutoCarComtTxtVw);

        Produto produto = (Produto) produtoCarrinhoCompras.get("produto");
        Integer quantidade = Convert.getInt(produtoCarrinhoCompras, "quantidade");
        BigDecimal valorSubTotal = produto.getPreco().multiply(new BigDecimal(quantidade));

        nomeProdutoCarCom.setText(produto.getNome());
        quantidadeProdutoCarCom.setText(Convert.toString(quantidade));
        subTotalProdutoCarCom.setText(UtilsApp.stringTwoDecimalPlaces(valorSubTotal));

        return view;
    }
}
