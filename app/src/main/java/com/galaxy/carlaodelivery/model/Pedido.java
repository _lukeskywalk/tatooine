package com.galaxy.carlaodelivery.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

public class Pedido implements Serializable{

    @SerializedName("codigo")
    private Integer codigo;

    @SerializedName("usuario_codigo")
    private Integer usuarioCodigo;

    @SerializedName("statuspedido_codigo")
    private Integer statusPedidoCodigo;

    @SerializedName("valor")
    private BigDecimal valor;

    @SerializedName("criadoem")
    private String criadoEm;

    @SerializedName("modificadoem")
    private String modificadoEm;

    @SerializedName("enderecoentrega")
    private String enderecoEntrega;

    @SerializedName("formapagamento_codigo")
    private Integer formaPagamentoCodigo;

    @SerializedName("usuario")
    private Usuario usuario;

    @SerializedName("status_pedido")
    private StatusPedido statusPedido;

    @SerializedName("forma_pagamento")
    private FormaPagamento formaPagamento;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getUsuarioCodigo() {
        return usuarioCodigo;
    }

    public void setUsuarioCodigo(Integer usuarioCodigo) {
        this.usuarioCodigo = usuarioCodigo;
    }

    public Integer getStatusPedidoCodigo() {
        return statusPedidoCodigo;
    }

    public void setStatusPedidoCodigo(Integer statusPedidoCodigo) {
        this.statusPedidoCodigo = statusPedidoCodigo;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getCriadoEm() {
        return criadoEm;
    }

    public void setCriadoEm(String criadoEm) {
        this.criadoEm = criadoEm;
    }

    public String getModificadoEm() {
        return modificadoEm;
    }

    public void setModificadoEm(String modificadoEm) {
        this.modificadoEm = modificadoEm;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public StatusPedido getStatusPedido() {
        return statusPedido;
    }

    public void setStatusPedido(StatusPedido statusPedido) {
        this.statusPedido = statusPedido;
    }

    public String getEnderecoEntrega() {
        return enderecoEntrega;
    }

    public void setEnderecoEntrega(String enderecoEntrega) {
        this.enderecoEntrega = enderecoEntrega;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public Integer getFormaPagamentoCodigo() {
        return formaPagamentoCodigo;
    }

    public void setFormaPagamentoCodigo(Integer formaPagamentoCodigo) {
        this.formaPagamentoCodigo = formaPagamentoCodigo;
    }
}
