package com.galaxy.carlaodelivery.model;

import com.google.gson.annotations.SerializedName;

public class PedidoProduto {

    @SerializedName("codigo")
    private Integer codigo;

    @SerializedName("pedido_codigo")
    private Integer pedidoCodigo;

    @SerializedName("produto_codigo")
    private Integer produtoCodigo;

    @SerializedName("quantidade")
    private Integer quantidade;

    @SerializedName("produto")
    private Produto produto;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getPedidoCodigo() {
        return pedidoCodigo;
    }

    public void setPedidoCodigo(Integer pedidoCodigo) {
        this.pedidoCodigo = pedidoCodigo;
    }

    public Integer getProdutoCodigo() {
        return produtoCodigo;
    }

    public void setProdutoCodigo(Integer produtoCodigo) {
        this.produtoCodigo = produtoCodigo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
}
