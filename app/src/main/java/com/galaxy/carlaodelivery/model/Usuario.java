package com.galaxy.carlaodelivery.model;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by edilson on 27/03/18.
 */

//@SuppressWarnings("serial")
public class Usuario implements Serializable{

    @SerializedName("cep")
    private String cep;

    @SerializedName("codigo")
    private Integer codigo;

    @SerializedName("data_cadastro")
    private String dataCadastro;

    @SerializedName("data_desativado")
    private String dataDesativado;

    @SerializedName("email")
    private String email;

    @SerializedName("enderecobairro")
    private String enderecoBairro;

    @SerializedName("enderecocomplemento")
    private String enderecoComplemento;

    @SerializedName("endereconumero")
    private String enderecoNumero;

    @SerializedName("enderecorua")
    private String enderecoRua;

    @SerializedName("nome")
    private String nome;

    @SerializedName("statususuario_codigo")
    private String statusUsuarioCodigo;

    @SerializedName("senha")
    private String senha;

    @SerializedName("ultima_senha")
    private String ultimaSenha;

    @SerializedName("usuario")
    private String usuario;

    @SerializedName("statususuario")
    private StatusUsuario statusUsuario;

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(String dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getDataDesativado() {
        return dataDesativado;
    }

    public void setDataDesativado(String dataDesativado) {
        this.dataDesativado = dataDesativado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnderecoBairro() {
        return enderecoBairro;
    }

    public void setEnderecoBairro(String enderecoBairro) {
        this.enderecoBairro = enderecoBairro;
    }

    public String getEnderecoComplemento() {
        return enderecoComplemento;
    }

    public void setEnderecoComplemento(String enderecoComplemento) {
        this.enderecoComplemento = enderecoComplemento;
    }

    public String getEnderecoNumero() {
        return enderecoNumero;
    }

    public void setEnderecoNumero(String enderecoNumero) {
        this.enderecoNumero = enderecoNumero;
    }

    public String getEnderecoRua() {
        return enderecoRua;
    }

    public void setEnderecoRua(String enderecoRua) {
        this.enderecoRua = enderecoRua;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getStatusUsuarioCodigo() {
        return statusUsuarioCodigo;
    }

    public void setStatusUsuarioCodigo(String statusUsuarioCodigo) {
        this.statusUsuarioCodigo = statusUsuarioCodigo;
    }

    public String getUltimaSenha() {
        return ultimaSenha;
    }

    public void setUltimaSenha(String ultimaSenha) {
        this.ultimaSenha = ultimaSenha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public StatusUsuario getStatusUsuario() {
        return statusUsuario;
    }

    public void setStatusUsuario(StatusUsuario statusUsuario) {
        this.statusUsuario = statusUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
