package com.galaxy.carlaodelivery.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by edilson on 28/03/18.
 */

public class APIResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("detalhe")
    private String detalhe;

    @SerializedName("grupo")
    private Grupo grupo;

    @SerializedName("grupo_permissao")
    private GrupoPermissao grupoPermissao;

    @SerializedName("grupo_usuario")
    private GrupoUsuario grupoUsuario;

    @SerializedName("parametro")
    private Parametro parametro;

    @SerializedName("pedido")
    private Pedido pedido;

    @SerializedName("pedido_produto")
    private PedidoProduto pedidoProduto;

    @SerializedName("permissao")
    private Permissao permissao;

    @SerializedName("produto")
    private Produto produto;

    @SerializedName("status_pedido")
    private StatusPedido statusPedido;

    @SerializedName("status_usuario")
    private StatusUsuario statusUsuario;

    @SerializedName("usuario")
    private Usuario usuario;

    @SerializedName("forma_pagamento")
    private FormaPagamento formaPagamento;

    @SerializedName("token_usuario")
    private TokenUsuario tokenUsuario;

    @SerializedName("list_permissao")
    private List<Permissao> listPermissao;

    @SerializedName("list_produtos")
    private List<Produto> listProduto;

    @SerializedName("list_forma_pagamentos")
    private List<FormaPagamento> listFormaPagamento;

    @SerializedName("list_pedidos")
    private List<Pedido> listPedido;

    @SerializedName("list_pedido_produto")
    private List<PedidoProduto> listPedidoProduto;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(String detalhe) {
        this.detalhe = detalhe;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public GrupoPermissao getGrupoPermissao() {
        return grupoPermissao;
    }

    public void setGrupoPermissao(GrupoPermissao grupoPermissao) {
        this.grupoPermissao = grupoPermissao;
    }

    public GrupoUsuario getGrupoUsuario() {
        return grupoUsuario;
    }

    public void setGrupoUsuario(GrupoUsuario grupoUsuario) {
        this.grupoUsuario = grupoUsuario;
    }

    public Parametro getParametro() {
        return parametro;
    }

    public void setParametro(Parametro parametro) {
        this.parametro = parametro;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public PedidoProduto getPedidoProduto() {
        return pedidoProduto;
    }

    public void setPedidoProduto(PedidoProduto pedidoProduto) {
        this.pedidoProduto = pedidoProduto;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public StatusPedido getStatusPedido() {
        return statusPedido;
    }

    public void setStatusPedido(StatusPedido statusPedido) {
        this.statusPedido = statusPedido;
    }

    public StatusUsuario getStatusUsuario() {
        return statusUsuario;
    }

    public void setStatusUsuario(StatusUsuario statusUsuario) {
        this.statusUsuario = statusUsuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public TokenUsuario getTokenUsuario() {
        return tokenUsuario;
    }

    public void setTokenUsuario(TokenUsuario tokenUsuario) {
        this.tokenUsuario = tokenUsuario;
    }

    public List<Permissao> getListPermissao() {
        return listPermissao;
    }

    public void setListPermissao(List<Permissao> listPermissao) {
        this.listPermissao = listPermissao;
    }

    public List<Produto> getListProduto() {
        return listProduto;
    }

    public void setListProduto(List<Produto> listProduto) {
        this.listProduto = listProduto;
    }

    public List<FormaPagamento> getListFormaPagamento() {
        return listFormaPagamento;
    }

    public void setListFormaPagamento(List<FormaPagamento> listFormaPagamento) {
        this.listFormaPagamento = listFormaPagamento;
    }

    public List<Pedido> getListPedido() {
        return listPedido;
    }

    public void setListPedido(List<Pedido> listPedido) {
        this.listPedido = listPedido;
    }

    public List<PedidoProduto> getListPedidoProduto() {
        return listPedidoProduto;
    }

    public void setListPedidoProduto(List<PedidoProduto> listPedidoProduto) {
        this.listPedidoProduto = listPedidoProduto;
    }
}
