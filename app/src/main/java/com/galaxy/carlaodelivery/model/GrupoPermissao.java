package com.galaxy.carlaodelivery.model;

import com.google.gson.annotations.SerializedName;

public class GrupoPermissao {

    @SerializedName("codigo")
    private Integer codigo;

    @SerializedName("grupo_codigo")
    private Integer grupoCodigo;

    @SerializedName("permissao_codigo")
    private Integer permissaoCodigo;

    @SerializedName("permissao")
    private Permissao permissao;

    @SerializedName("grupo")
    private Grupo grupo;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getGrupoCodigo() {
        return grupoCodigo;
    }

    public void setGrupoCodigo(Integer grupoCodigo) {
        this.grupoCodigo = grupoCodigo;
    }

    public Integer getPermissaoCodigo() {
        return permissaoCodigo;
    }

    public void setPermissaoCodigo(Integer permissaoCodigo) {
        this.permissaoCodigo = permissaoCodigo;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }
}
