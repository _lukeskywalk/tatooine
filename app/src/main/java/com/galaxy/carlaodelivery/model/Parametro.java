package com.galaxy.carlaodelivery.model;

import com.google.gson.annotations.SerializedName;

public class Parametro {

    @SerializedName("codigo")
    private Integer codigo;

    @SerializedName("parametro")
    private String parametro;

    @SerializedName("valor")
    private String valor;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
