package com.galaxy.carlaodelivery.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StatusPedido implements Serializable {

    @SerializedName("codigo")
    private Integer codigo;

    @SerializedName("nome")
    private String nome;

    public StatusPedido(){

    }

    public StatusPedido(Integer codigo, String nome){
        this.codigo = codigo;
        this.nome = nome;
    }

    public StatusPedido(String nome){
        this.nome = nome;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
