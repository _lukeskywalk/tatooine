package com.galaxy.carlaodelivery.model;

import com.google.gson.annotations.SerializedName;

public class TokenUsuario {
    @SerializedName("codigo")
    private Integer codigo;

    @SerializedName("usuario_codigo")
    private Integer usuarioCodigo;

    @SerializedName("token")
    private String token;

    @SerializedName("criadoem")
    private String criadoEm;

    @SerializedName("usuario")
    private Usuario usuario;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getUsuarioCodigo() {
        return usuarioCodigo;
    }

    public void setUsuarioCodigo(Integer usuarioCodigo) {
        this.usuarioCodigo = usuarioCodigo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCriadoEm() {
        return criadoEm;
    }

    public void setCriadoEm(String criadoEm) {
        this.criadoEm = criadoEm;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
