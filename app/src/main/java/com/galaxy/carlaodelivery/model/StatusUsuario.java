package com.galaxy.carlaodelivery.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by edilson on 28/03/18.
 */

public class StatusUsuario implements Serializable{

    @SerializedName("codigo")
    private Integer codigo;

    @SerializedName("nome")
    private String nome;

    public StatusUsuario(Integer codigo, String nome){
        this.codigo = codigo;
        this.nome = nome;
    }

    public StatusUsuario(String nome){
        this.nome = nome;
    }

    public StatusUsuario(){

    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
