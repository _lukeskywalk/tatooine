package com.galaxy.carlaodelivery.model;

import com.google.gson.annotations.SerializedName;

public class GrupoUsuario {

    @SerializedName("codigo")
    private Integer codigo;

    @SerializedName("grupo_codigo")
    private Integer grupoCodigo;

    @SerializedName("usuario_codigo")
    private Integer usuarioCodigo;

    @SerializedName("grupo")
    private Grupo grupo;

    @SerializedName("usuario")
    private Usuario usuario;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getGrupoCodigo() {
        return grupoCodigo;
    }

    public void setGrupoCodigo(Integer grupoCodigo) {
        this.grupoCodigo = grupoCodigo;
    }

    public Integer getUsuarioCodigo() {
        return usuarioCodigo;
    }

    public void setUsuarioCodigo(Integer usuarioCodigo) {
        this.usuarioCodigo = usuarioCodigo;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
