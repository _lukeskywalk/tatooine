package com.galaxy.carlaodelivery.http;

import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.PedidoProduto;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface APIInterface {
    @GET("/statusUsuario/")
    Call<APIResponse> getStatusUsuario(@Query("token") String token, @Query("idStatusUsuario") String idUsuario);

    @POST("/statusUsuario/")
    Call<APIResponse> postStatusUsuario(@Query("token") String token, @Query("nome") String nome);

    @PUT("/statusUsuario/")
    Call<APIResponse> putStatusUsuario(@Query("token") String token, @Query("codigo") String codigo, @Query("nome") String nome);

    @DELETE("/statusUsuario/")
    Call<APIResponse> deleteStatusUsuario(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/usuario/")
    Call<APIResponse> getUsuario(@Query("token") String token, @Query("idUsuario") String idUsuario);

    @GET("/usuarioByEmail/")
    Call<APIResponse> getUsuarioByEmail(@Query("token") String token, @Query("email") String email);

    @POST("/usuario/")
    Call<APIResponse> postUsuario(@Query("token") String token,
                                  @Query("usuario") String usuario,
                                  @Query("nome") String nome,
                                  @Query("senha") String senha,
                                  @Query("ultimaSenha") String ultimaSenha,
                                  @Query("statusUsuarioCodigo") String statusUsuarioCodigo,
                                  @Query("dataCadastro") String dataCadastro,
                                  @Query("dataDesativado") String dataDesativado,
                                  @Query("enderecoRua") String enderecoRua,
                                  @Query("enderecoNumero") String enderecoNumero,
                                  @Query("enderecoBairro") String enderecoBairro,
                                  @Query("enderecoComplemento") String enderecoComplemento,
                                  @Query("email") String email,
                                  @Query("cep") String cep);

    @PUT("/usuario/")
    Call<APIResponse> putUsuario(@Query("token") String token, @Query("codigo") String codigo,
                                 @Query("nome") String nome, @Query("usuario") String usuario,
                                 @Query("senha") String senha, @Query("ultimaSenha") String ultimaSenha,
                                 @Query("statusUsuarioCodigo") String statusUsuarioCodigo, @Query("dataCadastro") String dataCadastro,
                                 @Query("dataDesativado") String dataDesativado, @Query("enderecoRua") String enderecoRua,
                                 @Query("enderecoNumero") String enderecoNumero, @Query("enderecoBairro") String enderecoBairro,
                                 @Query("enderecoComplemento") String enderecoComplemento, @Query("email") String email,
                                 @Query("cep") String cep);

    @DELETE("/usuario/")
    Call<APIResponse> deleteUsuario(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/statusPedido/")
    Call<APIResponse> getStatusPedido(@Query("token") String token, @Query("idStatusPedido") String idStatusPedido);

    @GET("/statusPedidoByNome/")
    Call<APIResponse> getStatusPedidoByNome(@Query("token") String token, @Query("nome") String nome);

    @POST("/statusPedido/")
    Call<APIResponse> postStatusPedido(@Query("token") String token, @Query("nome") String nome);

    @PUT("/statusPedido/")
    Call<APIResponse> putStatusPedido(@Query("token") String token, @Query("codigo") String codigo,
                                      @Query("nome") String nome);

    @DELETE("/statusPedido/")
    Call<APIResponse> deleteStatusPedido(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/produto/")
    Call<APIResponse> getProduto(@Query("token") String token, @Query("idProduto") String idProduto);

    @GET("/produtos/")
    Call<APIResponse> getProdutos(@Query("token") String token);

    @GET("/produtosByNome/")
    Call<APIResponse> getProdutosByNome(@Query("token") String token, @Query("nome") String nome);

    @POST("/produto/")
    Call<APIResponse> postProduto(@Query("token") String token, @Query("nome") String nome,
                                  @Query("descricao") String descricao, @Query("estoque") String estoque,
                                  @Query("preco") String preco);

    @PUT("/produto/")
    Call<APIResponse> putProduto(@Query("token") String token, @Query("codigo") String codigo,
                                 @Query("nome") String nome, @Query("descricao") String descricao,
                                 @Query("estoque") String estoque, @Query("preco") String preco);

    @DELETE("produto")
    Call<APIResponse> deleteProduto(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/pedido/")
    Call<APIResponse> getPedido(@Query("token") String token, @Query("idPedido") String idPedido);

    @GET("/pedidosByUsuario/")
    Call<APIResponse> getPedidosByUsuarioCodigo(@Query("token") String token, @Query("usuarioCodigo") String usuarioCodigo);

    @POST("/pedido/")
    Call<APIResponse> postPedido(@Query("token") String token, @Query("usuarioCodigo") String usuarioCodigo,
                                 @Query("statusPedidoCodigo") String statusPedidoCodigo, @Query("valor") String valor,
                                 @Query("criadoEm") String criadoEm, @Query("modificadoEm") String modificadoEm,
                                 @Query("enderecoEntrega") String enderecoEntrega,
                                 @Query("formaPagamentoCodigo") String formaPagamentoCodigo);

    @PUT("/pedido/")
    Call<APIResponse> putPedido(@Query("token") String token, @Query("codigo") String codigo,
                                @Query("usuarioCodigo") String usuarioCodigo, @Query("statusPedidoCodigo") String statusPedidoCodigo,
                                @Query("valor") String valor, @Query("criadoEm") String criadoEm,
                                @Query("modificadoEm") String modificadoEm, @Query("formaPagamentoCodigo") String formaPagamentoCodigo);

    @DELETE("/pedido/")
    Call<APIResponse> deletePedido(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/pedidoProduto/")
    Call<APIResponse> getPedidoProduto(@Query("token") String token, @Query("idPedidoProduto") String idPedidoProduto);

    @GET("/pedidoProdutoByPedidoCodigo/")
    Call<APIResponse> getPedidoProdutoByPedidoCodigo(@Query("token") String token, @Query("pedido_codigo") String pedidoCodigo);

    @POST("/pedidoProduto/")
    Call<APIResponse> postPedidoProduto(@Query("token") String token, @Query("pedidoCodigo") String pedidoCodigo,
                                        @Query("produtoCodigo") String produtoCodigo, @Query("quantidade") String quantidade);

    @POST("/pedidoProdutoList/")
    Call<APIResponse> postPedidoProdutoList(@Query("token") String token, @Query("listPedidoProduto") List<PedidoProduto> listPedidoProduto);

    @PUT("/pedidoProduto/")
    Call<APIResponse> putPedidoProduto(@Query("token") String token, @Query("codigo") String codigo,
                                       @Query("pedidoCodigo") String pedidoCodigo, @Query("produtoCodigo") String produtoCodigo,
                                       @Query("quantidade") String quantidade);

    @DELETE("/pedidoProduto/")
    Call<APIResponse> deletePedidoProduto(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/parametro/")
    Call<APIResponse> getParametro(@Query("token") String token, @Query("idParametro") String idParametro);

    @POST("/parametro/")
    Call<APIResponse> postParametro(@Query("token") String token, @Query("parametro") String parametro,
                                    @Query("valor") String valor);

    @PUT("/parametro/")
    Call<APIResponse> putParametro(@Query("token") String token, @Query("codigo") String codigo,
                                   @Query("parametro") String parametro, @Query("valor") String valor);

    @DELETE("/parametro/")
    Call<APIResponse> deleteParametro(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/grupo/")
    Call<APIResponse> getGrupo(@Query("token") String token, @Query("idGrupo") String idGrupo);

    @POST("/grupo/")
    Call<APIResponse> postGrupo(@Query("token") String token, @Query("nome") String nome);

    @PUT("/grupo/")
    Call<APIResponse> putGrupo(@Query("token") String token, @Query("codigo") String codigo,
                               @Query("nome") String nome);

    @DELETE("/grupo/")
    Call<APIResponse> deleteGrupo(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/grupoUsuario/")
    Call<APIResponse> getGrupoUsuario(@Query("token") String token, @Query("idGrupoUsuario") String idGrupoUsuario);

    @POST("/grupoUsuario/")
    Call<APIResponse> postGrupoUsuario(@Query("token") String token, @Query("grupoCodigo") String grupoCodigo,
                                       @Query("usuarioCodigo") String usuarioCodigo);

    @PUT("/grupoUsuario/")
    Call<APIResponse> putGrupoUsuario(@Query("token") String token, @Query("codigo") String codigo,
                                      @Query("grupoCodigo") String grupoCodigo, @Query("usuarioCodigo") String usuarioCodigo);

    @DELETE("/grupoUsuario/")
    Call<APIResponse> deleteGrupoUsuario(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/permissao/")
    Call<APIResponse> getPermissao(@Query("token") String token, @Query("idPermissao") String idPermissao);

    @POST("/permissao/")
    Call<APIResponse> postPermissao(@Query("token") String token, @Query("nome") String nome);

    @PUT("/permissao/")
    Call<APIResponse> putPermissao(@Query("token") String token, @Query("codigo") String codigo,
                                   @Query("nome") String nome);

    @DELETE("/permissao/")
    Call<APIResponse> deletePermissao(@Query("token") String token, @Query("codigo") String codigo);

    @GET("grupoPermissao")
    Call<APIResponse> getGrupoPermissao(@Query("token") String token, @Query("idGrupoPermissao") String idGrupoPermissao);

    @POST("/grupoPermissao/")
    Call<APIResponse> postGrupoPermissao(@Query("token") String token, @Query("grupoCodigo") String grupoCodigo,
                                         @Query("permissaoCodigo") String permissaoCodigo);

    @PUT("/grupoPermissao/")
    Call<APIResponse> putGrupoPermissao(@Query("token") String token, @Query("codigo") String codigo,
                                        @Query("grupoCodigo") String grupoCodigo, @Query("permissaoCodigo") String permissaoCodigo);

    @DELETE("/grupoPermissao/")
    Call<APIResponse> deleteGrupoPermissao(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/formaPagamento/")
    Call<APIResponse> getFormaPagamento(@Query("token") String token, @Query("idFormaPagamento") String idFormaPagamento);

    @GET("/formaPagamentos/")
    Call<APIResponse> getFormaPagamentos(@Query("token") String token);

    @POST("/formaPagamento/")
    Call<APIResponse> postFormaPagamento(@Query("token") String token, @Query("nome") String nome);

    @PUT("/formaPagamento/")
    Call<APIResponse> putFormaPagamento(@Query("token") String token, @Query("codigo") String codigo,
                                   @Query("nome") String nome);

    @DELETE("/formaPagamento/")
    Call<APIResponse> deleteFormaPagamento(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/tokenUsuarioByToken/")
    Call<APIResponse> getTokenUsuarioByToken(@Query("token") String token, @Query("tokenUsuario") String tokenUsuario);

    @GET("/tokenUsuario/")
    Call<APIResponse> getTokenUsuario(@Query("token") String token, @Query("idTokenUsuario") String idTokenUsuario);

    @POST("/tokenUsuario/")
    Call<APIResponse> postTokenUsuario(@Query("token") String token, @Query("usuarioCodigo") String usuarioCodigo,
                                       @Query("usuarioToken") String usuarioToken, @Query("criadoEm") String criadoEm);

    @PUT("/tokenUsuario/")
    Call<APIResponse> putTokenUsuario(@Query("token") String token, @Query("codigo") String codigo,
                                      @Query("usuarioCodigo") String usuarioCodigo, @Query("usuarioToken") String usuarioToken,
                                      @Query("criadoEm") String criadoEm);

    @DELETE("/tokenUsuario/")
    Call<APIResponse> deleteTokenUsuario(@Query("token") String token, @Query("codigo") String codigo);

    @GET("/permissoesUsuario/")
    Call<APIResponse> getPermissoesUsuario(@Query("token") String token, @Query("usuarioCodigo") String usuarioCodigo);

    @GET("/info/")
    Call<APIResponse> getInfo();

    @POST("/feedbackpedido/")
    Call<APIResponse> postFeedbackPedido(@Query("token") String token, @Query("pedidoCodigo") String pedidoCodigo, @Query("texto") String texto);




}
