package com.galaxy.carlaodelivery.thread;

public interface ThreadCompleteListener {
    public void notifyOfThreadComplete(final Thread thread);
}
