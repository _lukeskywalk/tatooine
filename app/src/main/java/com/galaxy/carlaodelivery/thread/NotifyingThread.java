package com.galaxy.carlaodelivery.thread;

import android.content.Context;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public abstract class NotifyingThread extends Thread{
    private final Set<ThreadCompleteListener> listeners = new CopyOnWriteArraySet<>();

    private Context context;
    private String recipient;
    private int userCode;

    public final void addListener(final ThreadCompleteListener listener){
        listeners.add(listener);
    }

    public void removeListener(final ThreadCompleteListener listener){
        listeners.remove(listener);
    }

    private final void notifyListeners(){
        for(ThreadCompleteListener listener : listeners){
            listener.notifyOfThreadComplete(this);
        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public int getUserCode() {
        return userCode;
    }

    public void setUserCode(int userCode) {
        this.userCode = userCode;
    }

    @Override
    public final void run(){
        try{
            doRun(context, recipient, userCode);
        }finally {
            notifyListeners();
        }
    }

    public abstract void doRun(Context context, String recipient, int userCode);
}
