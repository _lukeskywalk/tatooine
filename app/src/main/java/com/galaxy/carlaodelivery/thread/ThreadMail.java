package com.galaxy.carlaodelivery.thread;

import android.content.Context;
import android.util.Log;

import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.mail.GMailSender;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.TokenUsuario;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.crypt.MD5;
import com.galaxy.korriban.util.Convert;

import java.util.Date;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ThreadMail extends NotifyingThread{

    @Override
    public void doRun(final Context context, final String recipient, int userCode) {
        try {
            final GMailSender sender = new GMailSender(Parameters.EMAIL_CARLAO,
                    Parameters.SENHA_EMAIL_CARLAO);

            String tokenUser = MD5.toMD5(Convert.toString(System.currentTimeMillis()) + userCode);


            String emailBody = "<span>" + UtilsApp.getString(context, "email_corpo_esqueci_senha") + "</span>";
            emailBody += "<br/>";
            String deepLink = UtilsApp.getString(context, "redirect_deep_link_app") + UtilsApp.getString(context, "deep_link_app") +"/" + tokenUser;
            emailBody += "<br/>";
            emailBody += "<a href=\"" + deepLink +"\">" + UtilsApp.getString(context, "mudar_senha") +"</a>";
            emailBody += "<br/>";
            emailBody += "<span>" + UtilsApp.getString(context, "lembre_se_abrir_link_no_celular_com_app_instalado") + "</span>";

            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<APIResponse> call = apiInterface.postTokenUsuario(UtilsApp.generateValidTokenEndorApi(),
                    UtilsApp.toBase64(userCode), UtilsApp.toBase64(tokenUser),
                    UtilsApp.toBase64(Convert.formatDate(new Date(),"dd/MM/yyyy HH:mm:ss")));

            final String finalEmailBody = emailBody;
            call.enqueue(new Callback<APIResponse>() {
                @Override
                public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                    APIResponse apiResponse = response.body();
                    if(apiResponse != null){
                        if(apiResponse.getStatus().equals("OK")){

                            new Thread(new Runnable(){
                                @Override
                                public void run() {
                                    try{
                                        sender.sendMail(UtilsApp.getString(context, "email_assunto_esqueci_senha"), finalEmailBody,
                                                Parameters.EMAIL_CARLAO, recipient);
                                    }catch(Exception ex){
                                        Log.e("SendMail", ex.getMessage(), ex);
                                        UtilsApp.shortToastFromStrings(context, "erro_enviar_email_esqueci_senha");
                                    }
                                }
                            }).start();



                        }else{
                            UtilsApp.shortToastFromStrings(context, "erro_enviar_email_esqueci_senha");
                        }
                    }else{
                        UtilsApp.shortToastFromStrings(context, "erro_enviar_email_esqueci_senha");
                    }
                }

                @Override
                public void onFailure(Call<APIResponse> call, Throwable t) {
                    UtilsApp.shortToastFromStrings(context, "erro_enviar_email_esqueci_senha");
                }
            });
        } catch (Exception e) {
            Log.e("SendMail", e.getMessage(), e);
        }
    }


}
