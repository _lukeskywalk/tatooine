package com.galaxy.carlaodelivery.ui;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.utils.UtilsApp;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CarrinhoComprasDialog extends Dialog{
    private Context mContext;
    private Spinner quantidadeSpnr;
    private List<String> mList;

    public interface DialogListener{
        public void ready(int n);
        public void cancelled();
    }

    private DialogListener mReadyListener;

    public CarrinhoComprasDialog(Context context, List<String> list, DialogListener readyListener){
        super(context);
        mReadyListener = readyListener;
        mContext = context;
        mList = new LinkedList<>();
        mList = list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.carrinho_compras_dialog);
        quantidadeSpnr = (Spinner) findViewById(R.id.quantidadeSpnrCarComDialog);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, mList);
        quantidadeSpnr.setAdapter(adapter);

        Button btnOk = (Button) findViewById(R.id.btnOKCarComDialog);
        Button btnCancelar = (Button) findViewById(R.id.btnCancelarCarComDialog);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n = quantidadeSpnr.getSelectedItemPosition();
                mReadyListener.ready(n);
                CarrinhoComprasDialog.this.dismiss();
            }
        });
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mReadyListener.cancelled();
                CarrinhoComprasDialog.this.dismiss();
            }
        });
    }
}
