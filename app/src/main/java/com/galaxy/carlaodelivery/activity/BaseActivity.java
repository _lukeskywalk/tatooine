package com.galaxy.carlaodelivery.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Permissao;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    public FrameLayout contentFrame;
    public TextView usuarioLogadoTxtVw;
    public TextView emailUsuarioLogadoTxtVw;
    public String usuarioLogado;
    public String emailUsuarioLogado;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        contentFrame = findViewById(R.id.content_frame);


        navigationView = findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        usuarioLogadoTxtVw = (TextView) headerLayout.findViewById(R.id.usuarioLogadoTxtVwNavHeader);
        emailUsuarioLogadoTxtVw = (TextView) headerLayout.findViewById(R.id.emailUsuarioLogadoTxtVwNavHeader);
        showDrawerItemsByPermissions();

        navigationView.setNavigationItemSelectedListener(
        new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                // set item as selected to persist highlight
                menuItem.setChecked(true);
                // close drawer when item is tapped
                mDrawerLayout.closeDrawers();

                switch (menuItem.getItemId()){
                    case R.id.meus_dados_base_drawer_item:
                        UtilsApp.goToActivity(getApplicationContext(), MeusDados.class);
                        break;
                    case R.id.produtos_base_drawer_item:
                        UtilsApp.goToActivity(getApplicationContext(), Produtos.class);
                        break;
                    case R.id.carrinho_compras_drawer_item:
                        UtilsApp.goToActivity(getApplicationContext(), CarrinhoCompras.class);
                        break;
                    case R.id.meus_pedidos_drawer_item:
                        UtilsApp.goToActivity(getApplicationContext(), MeusPedidos.class);
                        break;
                    case R.id.sair_drawer_item:
                        menuItem.setChecked(false);
                        UtilsApp.removeSavedString(getApplicationContext(), Parameters.SHARED_PREF_USUARIO_LOGADO);
                        UtilsApp.removeSavedString(getApplicationContext(), Parameters.SHARED_PREF_USER_PERMISSIONS);
                        UtilsApp.exitApp(BaseActivity.this, BaseActivity.class);
                        break;
                }
                return true;
            }
        });
    }

    private void showDrawerItemsByPermissions(){
        String savedUserPermissions = UtilsApp.getSavedString(getApplicationContext(), Parameters.SHARED_PREF_USER_PERMISSIONS);
        if(!savedUserPermissions.isEmpty()){
            String[] savedUserPermissionsArr = savedUserPermissions.split(";");
            Menu drawerMenu = navigationView.getMenu();
            for(int i = 0; i < drawerMenu.size(); i++){
                MenuItem drawerMenuItem = drawerMenu.getItem(i);
                drawerMenuItem.setVisible(false);
                for(String permissao : savedUserPermissionsArr){
                    String titulo = permissao.split("-")[0];
                    String acao = permissao.split("-")[1];

                    if(drawerMenuItem.getTitle().equals(titulo)){
                        if(acao.equals("Visualizar")){
                            drawerMenuItem.setVisible(true);
                            break;
                        }
                    }

                    if(drawerMenuItem.getTitle().equals(UtilsApp.getString(getApplicationContext(),"sair"))){
                        drawerMenuItem.setVisible(true);
                        break;
                    }
                }
            }

        }else{
            List<Permissao> listPermissoesString = new LinkedList<>();
            /*load user permissions*/
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<APIResponse> call = apiInterface.getPermissoesUsuario(
                    UtilsApp.generateValidTokenEndorApi(),
                    UtilsApp.toBase64(UtilsApp.getSavedInt(getApplicationContext(), Parameters.SHARED_PREF_CODIGO_USUARIO_LOGADO))
            );

            call.enqueue(new Callback<APIResponse>() {
                @Override
                public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                    APIResponse apiResponse = response.body();
                    if (apiResponse.getStatus().equals("OK")){
                        List<Permissao> listPermissao = apiResponse.getListPermissao();

                        if(listPermissao != null){
                            saveUserPermissions(listPermissao);
                            Menu drawerMenu = navigationView.getMenu();
                            for(int i = 0; i < drawerMenu.size(); i++){
                                MenuItem drawerMenuItem = drawerMenu.getItem(i);
                                drawerMenuItem.setVisible(false);
                                for(Permissao permissao : listPermissao){
                                    String titulo = permissao.getNome().split("-")[0];
                                    String acao = permissao.getNome().split("-")[1];

                                    if(drawerMenuItem.getTitle().equals(titulo)){
                                        if(acao.equals("Visualizar")){
                                            drawerMenuItem.setVisible(true);
                                            break;
                                        }
                                    }

                                    if(drawerMenuItem.getTitle().equals(UtilsApp.getString(getApplicationContext(),"sair"))){
                                        drawerMenuItem.setVisible(true);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResponse> call, Throwable t) {
                    Log.d("GET USER PERMISSIONS", t.getMessage());
                }
            });
        }
    }


    private void saveUserPermissions(List<Permissao> listaPermissao){
        String permissoes = "";

        for(Permissao permissao : listaPermissao){
            permissoes += permissao.getNome() + ";";
        }

        UtilsApp.saveString(getApplicationContext(), Parameters.SHARED_PREF_USER_PERMISSIONS, permissoes);
    }
}