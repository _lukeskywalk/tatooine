package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.list.adapter.CarrinhoComprasAdapter;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Parametro;
import com.galaxy.carlaodelivery.model.Produto;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.CarrinhoComprasSingleton;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.util.Convert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinalizarDadosPedido extends BaseActivity{

    private ListView produtosCarrinhoCompras;
    private TextView valorPrecoEntregaCarrinhoCompras;
    private Button btnConfirmarEndereco;
    private Button btnVoltarCarrinhoCompras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_finalizar_dados_pedido,null, false);
        contentFrame.addView(cv);

        produtosCarrinhoCompras = (ListView) findViewById(R.id.produtosCarrinhoComprasLtVwAtFinPed);
        btnConfirmarEndereco = (Button) findViewById(R.id.btnConfirmarEnderecoAtFinPed);
        btnVoltarCarrinhoCompras = (Button) findViewById(R.id.btnVoltarCarrinhoComprasAtFinPed);


        CarrinhoComprasAdapter carrinhoComprasAdapter = new CarrinhoComprasAdapter(CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao, FinalizarDadosPedido.this);
        produtosCarrinhoCompras.setAdapter(carrinhoComprasAdapter);


        btnVoltarCarrinhoCompras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilsApp.closeActivity(FinalizarDadosPedido.this);
                UtilsApp.goToActivity(getApplicationContext(), CarrinhoCompras.class);
            }
        });

        btnConfirmarEndereco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                Call<APIResponse> call = apiInterface.getUsuario(
                        UtilsApp.generateValidTokenEndorApi(),
                        UtilsApp.toBase64(
                                UtilsApp.getSavedInt(getApplicationContext(),
                                        Parameters.SHARED_PREF_CODIGO_USUARIO_LOGADO)
                        )
                );

                call.enqueue(new Callback<APIResponse>() {
                    @Override
                    public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                        APIResponse apiResponse = response.body();
                        if(apiResponse.getStatus().equals("OK")){
                            Usuario usuario = apiResponse.getUsuario();
                            Intent intent = new Intent(getApplicationContext(), FinalizarDadosEnderecoPedido.class);
                            intent.putExtra("usuario", usuario);
                            UtilsApp.closeActivity(FinalizarDadosPedido.this);
                            UtilsApp.goToActivity(getApplicationContext(), intent);
                        }else{
                            UtilsApp.closeActivity(FinalizarDadosPedido.this);
                            UtilsApp.goToActivity(getApplicationContext(), FinalizarDadosEnderecoPedido.class);
                        }
                    }

                    @Override
                    public void onFailure(Call<APIResponse> call, Throwable t) {
                        UtilsApp.closeActivity(FinalizarDadosPedido.this);
                        UtilsApp.goToActivity(getApplicationContext(), FinalizarDadosEnderecoPedido.class);
                    }
                });
            }
        });

    }

    private BigDecimal calcularSubTotalCompra(){
        BigDecimal subTotal = new BigDecimal(0);
        for(Map<String, Object> map : CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao){
            Produto produto = (Produto) map.get("produto");
            Integer quantidade = Convert.getInt(map,"quantidade");
            BigDecimal valorQuantidadeProduto = produto.getPreco().multiply(new BigDecimal(quantidade));
            subTotal = subTotal.add(valorQuantidadeProduto);
        }

        return subTotal;
    }
}
