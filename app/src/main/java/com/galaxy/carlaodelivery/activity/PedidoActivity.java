package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.list.adapter.PedidoProdutoAdapter;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Pedido;
import com.galaxy.carlaodelivery.model.PedidoProduto;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.util.Convert;

import org.w3c.dom.Text;

import java.util.List;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PedidoActivity extends BaseActivity {

    private TextView codigoPedidoTxtVw;
    private TextView dataPedidoTxtVw;
    private TextView statusPedidoTxtVw;
    private ListView pedidoProdutoLtVw;
    private TextView valorTotalTxtVw;
    private Button btnFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_pedido,null, false);
        contentFrame.addView(cv);

        codigoPedidoTxtVw = (TextView) findViewById(R.id.codigoPedidoTxtVwAtPedido);
        dataPedidoTxtVw = (TextView) findViewById(R.id.dataPedidoTxtVwAtPedido);
        statusPedidoTxtVw = (TextView) findViewById(R.id.statusPedidoTxtVwAtPedido);
        pedidoProdutoLtVw = (ListView) findViewById(R.id.pedidoProdutoLtVwAtPed);
        valorTotalTxtVw = (TextView) findViewById(R.id.valorTotalTxtVwAtPedido);
        btnFeedback = (Button) findViewById(R.id.btnFeedbackAtPedido);

        carregarDadosPedido();

        btnFeedback.setEnabled(false);

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentPedido= getIntent();
                Pedido pedido = (Pedido)intentPedido.getSerializableExtra("pedido");
                Intent intent = new Intent(getApplicationContext(), FeedbackPedido.class);
                intent.putExtra("pedido", pedido);
                UtilsApp.closeActivity(PedidoActivity.this);
                UtilsApp.goToActivity(getApplicationContext(), intent);
            }
        });
    }


    private void carregarDadosPedido(){
        Intent intent = getIntent();
        final Pedido pedido = (Pedido) intent.getSerializableExtra("pedido");
        codigoPedidoTxtVw.setText(Convert.toString(pedido.getCodigo()));
        statusPedidoTxtVw.setText(pedido.getStatusPedido().getNome());
        valorTotalTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces(pedido.getValor())));

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getPedidoProdutoByPedidoCodigo(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(pedido.getCodigo())
        );

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                if(apiResponse.getStatus().equals("OK")){
                    List<PedidoProduto> listPedidoProduto = apiResponse.getListPedidoProduto();
                    PedidoProdutoAdapter pedidoProdutoAdapter = new PedidoProdutoAdapter(listPedidoProduto, PedidoActivity.this);
                    pedidoProdutoLtVw.setAdapter(pedidoProdutoAdapter);
                    btnFeedback.setEnabled(true);

                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_dados_pedido");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_dados_pedido");
            }
        });
    }

}
