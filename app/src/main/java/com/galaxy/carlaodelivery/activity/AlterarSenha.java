package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.crypt.MD5;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlterarSenha extends BaseActivity {

    private EditText senhaEdtTxt;
    private EditText novaSenhaEdtTxt;
    private EditText novaSenhaNovamentEdtTxt;
    private Button btnOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_alterar_senha, null, false
        );
        contentFrame.addView(cv);

        senhaEdtTxt = (EditText) findViewById(R.id.senhaEdTxtAtAlterarSenha);
        novaSenhaEdtTxt = (EditText) findViewById(R.id.novaSenhaEdTxtAtAlterarSenha);
        novaSenhaNovamentEdtTxt = (EditText) findViewById(R.id.novaSenhaNovamenteEdTxtAtAlterarSenha);
        btnOK = (Button) findViewById(R.id.btnOkAtAlterarSenha);

        Intent intent = getIntent();
        final Usuario usuario = (Usuario) intent.getSerializableExtra("usuario");

        UtilsApp.abrirConexao();

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String senha = senhaEdtTxt.getText().toString();
                String novaSenha = novaSenhaEdtTxt.getText().toString();
                String novaSenhaNovamente = novaSenhaNovamentEdtTxt.getText().toString();

                if(UtilsApp.checkStringValuesFilled()){
                    if(MD5.toMD5(senha).equals(usuario.getSenha())){
                        if(novaSenha.equals(novaSenhaNovamente)){
                            usuario.setUltimaSenha(usuario.getSenha());
                            usuario.setSenha(MD5.toMD5(novaSenha));

                            alterarSenhaUsuario(usuario);
                        }else{
                            UtilsApp.shortToastFromStrings(getApplicationContext(), "senhas_nao_coincidem");
                        }
                    }else{
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "senha_incorreta");
                    }
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_todos_campos");
                }
            }
        });

    }

    private void alterarSenhaUsuario(Usuario usuario){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.putUsuario(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(usuario.getCodigo()),
                UtilsApp.toBase64(usuario.getNome()),
                UtilsApp.toBase64(usuario.getUsuario()),
                UtilsApp.toBase64(usuario.getSenha()),
                UtilsApp.toBase64(usuario.getUltimaSenha()),
                UtilsApp.toBase64(usuario.getStatusUsuarioCodigo()),
                UtilsApp.toBase64(UtilsApp.getDateWithLocale(usuario.getDataCadastro(), Parameters.STANDARD_DATE_FORMAT_SIMPLE)),
                UtilsApp.toBase64(UtilsApp.getDateWithLocale(usuario.getDataDesativado(), Parameters.STANDARD_DATE_FORMAT_SIMPLE)),
                UtilsApp.toBase64(usuario.getEnderecoRua()),
                UtilsApp.toBase64(usuario.getEnderecoNumero()),
                UtilsApp.toBase64(usuario.getEnderecoBairro()),
                UtilsApp.toBase64(usuario.getEnderecoComplemento()),
                UtilsApp.toBase64(usuario.getEmail()),
                UtilsApp.toBase64(usuario.getCep())

        );

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                if(apiResponse.getStatus().equals("OK")){
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "senha_alterada_sucesso");
                    UtilsApp.sleep(500);
                    UtilsApp.closeActivity(AlterarSenha.this);
                    UtilsApp.goToActivity(getApplicationContext(), MeusDados.class);
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(),"erro_trocar_senha");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(),"erro_trocar_senha");
            }
        });


    }
}
