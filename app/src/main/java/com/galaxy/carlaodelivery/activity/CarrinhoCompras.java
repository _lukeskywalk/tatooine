package com.galaxy.carlaodelivery.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.list.adapter.CarrinhoComprasAdapter;
import com.galaxy.carlaodelivery.model.Produto;
import com.galaxy.carlaodelivery.ui.CarrinhoComprasDialog;
import com.galaxy.carlaodelivery.utils.CarrinhoComprasSingleton;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.util.Convert;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CarrinhoCompras extends BaseActivity {

    private ListView produtosCarrinhoComprasLtVw;
    private TextView valorSubtTotalTxtVw;
    private Button btnFinalizarCompra;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_carrinho_compras,null, false);
        contentFrame.addView(cv);

        produtosCarrinhoComprasLtVw = (ListView) findViewById(R.id.produtosCarrinhoComprasLtVwAtCarCom);
        valorSubtTotalTxtVw = (TextView) findViewById(R.id.valorSubtotalTxtVwAtCarCom);
        btnFinalizarCompra = (Button) findViewById(R.id.btnFinalizarCompraAtCarCom);

        CarrinhoComprasAdapter carrinhoComprasAdapter = new CarrinhoComprasAdapter(CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao,CarrinhoCompras.this);
        produtosCarrinhoComprasLtVw.setAdapter(carrinhoComprasAdapter);
        produtosCarrinhoComprasLtVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {


                final List<String> arrayQuantidade = Arrays.asList(getResources().getStringArray(R.array.list_quantidade));
                CarrinhoComprasDialog carrinhoComprasDialog = new CarrinhoComprasDialog(CarrinhoCompras.this,arrayQuantidade, new CarrinhoComprasDialog.DialogListener() {
                    @Override
                    public void ready(int n) {
                        Map<String,Object> mapProdutoCarrinho = CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao.get(i);
                        Produto produtoCarrinho = (Produto) mapProdutoCarrinho.get("produto");

                        if(produtoCarrinho.getEstoque() < Integer.valueOf(arrayQuantidade.get(n))){
                            UtilsApp.shortToastFromStrings(getApplicationContext(),"desculpe_nao_ha_quantidade_estoque");
                        }else{
                            CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao.get(i).put("quantidade", arrayQuantidade.get(n));

                            CarrinhoComprasAdapter carrinhoComprasAdapter = new CarrinhoComprasAdapter(CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao,CarrinhoCompras.this);
                            produtosCarrinhoComprasLtVw.setAdapter(carrinhoComprasAdapter);


                            valorSubtTotalTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces(calcularSubTotalCompra())));
                        }
                    }

                    @Override
                    public void cancelled() {
                        System.out.println("cancelado");
                    }
                });

                carrinhoComprasDialog.show();
            }
        });

        valorSubtTotalTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces(calcularSubTotalCompra())));


        btnFinalizarCompra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilsApp.closeActivity(CarrinhoCompras.this);
                UtilsApp.goToActivity(getApplicationContext(), FinalizarDadosPedido.class);

            }
        });


    }


    private BigDecimal calcularSubTotalCompra(){
        BigDecimal subTotal = new BigDecimal(0);
        for(Map<String, Object> map : CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao){
            Produto produto = (Produto) map.get("produto");
            Integer quantidade = Convert.getInt(map,"quantidade");
            BigDecimal valorQuantidadeProduto = produto.getPreco().multiply(new BigDecimal(quantidade));
            subTotal = subTotal.add(valorQuantidadeProduto);
        }

        return subTotal;
    }
}
