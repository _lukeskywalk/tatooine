package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.UtilsApp;

public class FinalizarDadosEnderecoPedido extends BaseActivity {

    private EditText ruaEntregaEdtTxt;
    private EditText numeroEntregaEdtTxt;
    private EditText bairroEntregaEdtTxt;
    private EditText complementoEntregaEdtTxt;
    private EditText cepEntregaEdtTxt;
    private Button btnVoltarFinalizarDadosPedido;
    private Button btnConfirmarPedido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_finalizar_dados_endereco_pedido,null, false);
        contentFrame.addView(cv);

        ruaEntregaEdtTxt = (EditText) findViewById(R.id.ruaEntregaEdtTxtFinDadEndPed);
        numeroEntregaEdtTxt = (EditText) findViewById(R.id.numeroEntregaEdtTxtFinDadEndPed);
        bairroEntregaEdtTxt = (EditText) findViewById(R.id.bairroEntregaEdtTxtFinDadEndPed);
        complementoEntregaEdtTxt = (EditText) findViewById(R.id.complementoEntregaEdtTxtFinDadEndPed);
        cepEntregaEdtTxt = (EditText) findViewById(R.id.cepEntregaEdtTxtFinDadEndPed);
        btnVoltarFinalizarDadosPedido = (Button) findViewById(R.id.btnVoltarFinalizarDadosPedidoAtFinDadEndPed);
        btnConfirmarPedido = (Button) findViewById(R.id.btnConfirmarPedidoAtFinDadEndPed);


        final Intent intent = getIntent();
        final Usuario usuario = (Usuario)intent.getSerializableExtra("usuario");


        if(usuario != null){
            ruaEntregaEdtTxt.setText(usuario.getEnderecoRua());
            numeroEntregaEdtTxt.setText(usuario.getEnderecoNumero());
            bairroEntregaEdtTxt.setText(usuario.getEnderecoBairro());
            complementoEntregaEdtTxt.setText(usuario.getEnderecoComplemento());
            cepEntregaEdtTxt.setText(usuario.getCep());
        }

        btnVoltarFinalizarDadosPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilsApp.closeActivity(FinalizarDadosEnderecoPedido.this);
                UtilsApp.goToActivity(getApplicationContext(), FinalizarDadosPedido.class);
            }
        });

        btnConfirmarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String rua = ruaEntregaEdtTxt.getText().toString();
                String numero = numeroEntregaEdtTxt.getText().toString();
                String bairro = bairroEntregaEdtTxt.getText().toString();
                String complemento = complementoEntregaEdtTxt.getText().toString();
                String cep = cepEntregaEdtTxt.getText().toString();

                if(UtilsApp.checkStringValuesFilled(rua, numero, bairro, complemento, cep)){
                    if(UtilsApp.isValidCep(cep)){
                        Intent intent1 = new Intent(getApplicationContext(), FinalizarPedido.class);
                        intent1.putExtra("usuario", usuario);
                        intent1.putExtra("rua", rua);
                        intent1.putExtra("numero", numero);
                        intent1.putExtra("bairro", bairro);
                        intent1.putExtra("complemento", complemento);
                        intent1.putExtra("cep", cep);
                        UtilsApp.closeActivity(FinalizarDadosEnderecoPedido.this);
                        UtilsApp.goToActivity(getApplicationContext(), intent1);
                    }else{
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_cep_valido");
                    }
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_todos_campos");
                }
            }
        });


    }
}
