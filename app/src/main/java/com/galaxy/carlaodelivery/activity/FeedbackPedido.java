package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Pedido;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.UtilsApp;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackPedido extends BaseActivity {

    private EditText feedbackEdtTxt;
    private Button btnFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_feedback_pedido,null, false);
        contentFrame.addView(cv);

        feedbackEdtTxt = (EditText) findViewById(R.id.feedbackEdTxtAtFeedbackPedido);
        btnFeedback = (Button) findViewById(R.id.btnFeedbackAtFeedbackPedido);

        Intent intent = getIntent();
        final Pedido pedido = (Pedido) intent.getSerializableExtra("pedido");

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textoFeedback = feedbackEdtTxt.getText().toString();
                if(UtilsApp.checkStringValuesFilled(textoFeedback)){
                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    Call<APIResponse> call = apiInterface.postFeedbackPedido(
                            UtilsApp.generateValidTokenEndorApi(),
                            UtilsApp.toBase64(pedido.getCodigo()),
                            UtilsApp.toBase64(textoFeedback)
                    );

                    call.enqueue(new Callback<APIResponse>() {
                        @Override
                        public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                            APIResponse apiResponse = response.body();
                            if(apiResponse.getStatus().equals("OK")){
                                UtilsApp.shortToastFromStrings(getApplicationContext(), "feedback_enviado_sucesso");
                                UtilsApp.closeActivity(FeedbackPedido.this);
                                UtilsApp.goToActivity(getApplicationContext(), Home.class);
                            }else{
                                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_enviar_feedback");
                            }
                        }

                        @Override
                        public void onFailure(Call<APIResponse> call, Throwable t) {
                            UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_enviar_feedback");
                        }
                    });
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_todos_campos");
                }
            }
        });

    }

    private void enviarFeedbackPedido(Integer pedidoCodigo){

    }
}
