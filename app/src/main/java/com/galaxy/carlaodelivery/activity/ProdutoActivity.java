package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.model.Produto;
import com.galaxy.carlaodelivery.utils.CarrinhoComprasSingleton;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.util.Convert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ProdutoActivity extends BaseActivity {

    private TextView nomeProdutoTxtVw;
    private ImageView imagemProdutoImgVw;
    private TextView descricaoProdutoTxtVw;
    private TextView precoProdutoTxtVw;
    private Button btnComprar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_produto, null, false);
        contentFrame.addView(cv);

        nomeProdutoTxtVw = (TextView) findViewById(R.id.nomeProdutoTxtVwAtProduto);
        imagemProdutoImgVw = (ImageView) findViewById(R.id.imagemProdutoImgVwAtProduto);
        descricaoProdutoTxtVw = (TextView) findViewById(R.id.descricaoProdutoTxtVwAtProduto);
        precoProdutoTxtVw = (TextView) findViewById(R.id.precoProdutoTxtVwAtProduto);
        btnComprar = (Button) findViewById(R.id.btnComprarAtProduto);

        Intent intent = getIntent();
        final Produto produto = (Produto) intent.getSerializableExtra("produto");

        if (produto != null) {
            nomeProdutoTxtVw.setText(produto.getNome());
            descricaoProdutoTxtVw.setText(produto.getDescricao());
            precoProdutoTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces(produto.getPreco())));
            carregarImagemProduto(produto.getCodigo());
        }

        btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> mapProdutoComprar = new HashMap<>();
                mapProdutoComprar.put("codigo", produto.getCodigo());
                mapProdutoComprar.put("produto", produto);
                int quantidadeProduto = 1;

                boolean existe = false;
                Map<String, Object> mapProdutoExistente = null;
                for(Map<String, Object> map : CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao){
                    Integer codigoProdutoCarrinho = Convert.toInteger(map.get("codigo"));

                    if(produto.getCodigo().equals(codigoProdutoCarrinho)){
                        existe = true;
                        mapProdutoExistente = map;
                        quantidadeProduto = Convert.toInteger(map.get("quantidade")) + 1;
                        break;
                    }
                }

                if(existe){
                    CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao.remove(mapProdutoExistente);
                }

                mapProdutoComprar.put("quantidade", quantidadeProduto);
                CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao.add(mapProdutoComprar);

                UtilsApp.closeActivity(ProdutoActivity.this);
                UtilsApp.goToActivity(getApplicationContext(), CarrinhoCompras.class);

            }
        });

    }

    private void carregarImagemProduto(int codigoProduto) {

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(Parameters.BASE_URL + "/" + Parameters.IMAGEM_PRODUTO_URL + "?token=" + UtilsApp.generateValidTokenEndorApi() + "&produtoCodigo=" + UtilsApp.toBase64(codigoProduto))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_imagem_produto");
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                ProdutoActivity.this.runOnUiThread(new Runnable() {
                    Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());

                    public void run() {
                        imagemProdutoImgVw.setImageBitmap(bmp);
                    }
                });
            }
        });
    }
}
