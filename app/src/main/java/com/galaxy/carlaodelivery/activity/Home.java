package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;

public class Home extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_home,null, false);
        contentFrame.addView(cv);

        usuarioLogadoTxtVw.setText(UtilsApp.getSavedString(getApplicationContext(), Parameters.SHARED_PREF_USUARIO_LOGADO));
        emailUsuarioLogadoTxtVw.setText(UtilsApp.getSavedString(getApplicationContext(), Parameters.SHARED_PREF_EMAIL_USUARIO_LOGADO));

    }
}
