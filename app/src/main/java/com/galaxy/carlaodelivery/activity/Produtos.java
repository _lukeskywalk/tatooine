package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.list.adapter.ProdutoAdapter;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Produto;
import com.galaxy.carlaodelivery.utils.UtilsApp;

import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Produtos extends BaseActivity{
    private EditText pesquisarEdtTxt;
    private ListView produtosLtVw;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_produtos,null, false);
        contentFrame.addView(cv);

        pesquisarEdtTxt = (EditText) findViewById(R.id.pesquisarEdTxtAtProd);
        produtosLtVw = (ListView) findViewById(R.id.produtosLtVwAtProd);

        pesquisarEdtTxt.setEnabled(false);
        UtilsApp.abrirConexao();

        carregarListProdutos();

        pesquisarEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence != null){
                    if(!charSequence.toString().isEmpty()) {
                        carregarListProdutosCampoBusca(charSequence.toString());
                    }else{
                        carregarListProdutos();
                    }
                }else{
                    carregarListProdutos();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void carregarListProdutos(){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getProdutos(UtilsApp.generateValidTokenEndorApi());
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                if(apiResponse.getStatus().equals("OK")){

                    pesquisarEdtTxt.setEnabled(true);

                    final List<Produto> listProdutos = apiResponse.getListProduto();
                    ProdutoAdapter produtoAdapter = new ProdutoAdapter(listProdutos, Produtos.this);
                    produtosLtVw.setAdapter(produtoAdapter);
                    produtosLtVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            Intent intent = new Intent(getApplicationContext(), ProdutoActivity.class);
                            intent.putExtra("produto", listProdutos.get(position));

                            UtilsApp.closeActivity(Produtos.this);
                            UtilsApp.goToActivity(getApplicationContext(), intent);
                        }
                    });

                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_produtos");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_produtos");
            }
        });
    }

    private void carregarListProdutosCampoBusca(String busca){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getProdutosByNome(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(busca)
        );
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                if(apiResponse.getStatus().equals("OK")){
                    final List<Produto> listProdutos = apiResponse.getListProduto();
                    ProdutoAdapter produtoAdapter = new ProdutoAdapter(listProdutos, Produtos.this);
                    produtosLtVw.setAdapter(produtoAdapter);
                    produtosLtVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            Intent intent = new Intent(getApplicationContext(), ProdutoActivity.class);
                            intent.putExtra("produto", listProdutos.get(position));

                            UtilsApp.closeActivity(Produtos.this);
                            UtilsApp.goToActivity(getApplicationContext(), intent);
                        }
                    });

                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_produtos");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_produtos");
            }
        });
    }
}
