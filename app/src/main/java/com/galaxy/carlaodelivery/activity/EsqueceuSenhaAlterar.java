package com.galaxy.carlaodelivery.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.crypt.MD5;
import com.galaxy.korriban.util.Convert;

import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EsqueceuSenhaAlterar extends AppCompatActivity{
    private String userTokenPassword;
    private EditText novaSenhaEdtTxt;
    private EditText novaSenhaNovamenteEdtTxt;
    private Button btnOk;
    private Usuario usuarioVerificado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esqueceu_senha_alterar);

        userTokenPassword = getTokenPassword(this.getIntent());

        validateToken(userTokenPassword);

        novaSenhaEdtTxt = (EditText) findViewById(R.id.novaSenhaEdTxtAtEsqueceuSenhaAlterar);
        novaSenhaNovamenteEdtTxt = (EditText) findViewById(R.id.novaSenhaNovamenteEdtTxtEsqueceuSenhaAlterar);
        btnOk = (Button) findViewById(R.id.btnOkAtEsqueceuSenhaAlterar);

        UtilsApp.abrirConexao();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(usuarioVerificado != null){
                    String novaSenha = novaSenhaEdtTxt.getText().toString();
                    String novaSenhaNovamente = novaSenhaNovamenteEdtTxt.getText().toString();
                    if(UtilsApp.checkStringValuesFilled(novaSenha, novaSenhaNovamente)){
                        if(novaSenha.equals(novaSenhaNovamente)){
                            if(novaSenha.length() >= Parameters.MINIMUM_SIZE_PASSWORD && novaSenhaNovamente.length() >= Parameters.MINIMUM_SIZE_PASSWORD){
                                alterarSenhaUsuario(novaSenha);
                            }else{
                                UtilsApp.shortToastFromStrings(getApplicationContext(), "senha_deve_ser_maior_seis_caracteres");
                            }
                        }else{
                            UtilsApp.shortToastFromStrings(getApplicationContext(), "senhas_nao_coincidem");
                        }

                    }else{
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_todos_campos");
                    }
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_trocar_senha");
                }
            }
        });


    }

    private String getTokenPassword(Intent intent){
        String retorno = "";
        if(intent != null) {
            if (intent.getData() != null) {
                if (intent.getData().isHierarchical()) {
                    String uriString = intent.getDataString();
                    String arr[] = uriString.split(UtilsApp.getString(getApplicationContext(), "deep_link_app")+"/");
                    retorno = arr[1];
                }
            }
        }
        return retorno;
    }

    private void validateToken(final String token){

        if(token != null){
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<APIResponse> call = apiInterface.getTokenUsuarioByToken(UtilsApp.generateValidTokenEndorApi(),
                    UtilsApp.toBase64(token));
            call.enqueue(new Callback<APIResponse>() {
                @Override
                public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                    APIResponse apiResponse = response.body();

                    boolean sucesso = true;

                    if(apiResponse != null){
                        if(apiResponse.getTokenUsuario() != null){
                            Date dateCriado = UtilsApp.getDateWithLocal(apiResponse.getTokenUsuario().getCriadoEm());//Convert.toDate(apiResponse.getTokenUsuario().getCriadoEm(), Parameters.STANDARD_DATE_FORMAT_RETURN_API, Locale.ENGLISH);

                            long timeMilliCreated = dateCriado.getTime();
                            int minutes_duration_token = UtilsApp.getSavedInt(getApplicationContext(), Parameters.SHARED_PREF_MINUTES_DURATION_TOKEN_FORGOT_PASSWORD);

                            long tokenExpirationMilli = timeMilliCreated + (minutes_duration_token * 1000 * 60);

                            /*data de expiração maior que agora, sucesso*/
                            sucesso = tokenExpirationMilli >=  System.currentTimeMillis();

                        }else{
                            sucesso = false;
                        }
                    }else{
                        sucesso = false;
                    }

                    if(!sucesso){
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "link_esqueceu_senha_expirou_gere_outro");
                        UtilsApp.sleep(500);
                        UtilsApp.exitApp(EsqueceuSenhaAlterar.this, EsqueceuSenhaAlterar.class);
                    }else{
                        usuarioVerificado = apiResponse.getTokenUsuario().getUsuario();
                        if(usuarioVerificado == null){
                            UtilsApp.shortToastFromStrings(getApplicationContext(), "link_esqueceu_senha_expirou_gere_outro");
                            UtilsApp.sleep(500);
                            UtilsApp.exitApp(EsqueceuSenhaAlterar.this, EsqueceuSenhaAlterar.class);
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResponse> call, Throwable t) {
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "link_esqueceu_senha_expirou_gere_outro");
                    UtilsApp.sleep(500);
                    UtilsApp.exitApp(EsqueceuSenhaAlterar.this, EsqueceuSenhaAlterar.class);
                }
            });



        }
    }

    private void alterarSenhaUsuario(String novaSenha){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<APIResponse> call = apiInterface.putUsuario(
                UtilsApp.generateValidTokenEndorApi(), UtilsApp.toBase64(usuarioVerificado.getCodigo()),
                UtilsApp.toBase64(usuarioVerificado.getNome()), UtilsApp.toBase64(usuarioVerificado.getUsuario()),
                UtilsApp.toBase64(MD5.toMD5(novaSenha)), UtilsApp.toBase64(usuarioVerificado.getSenha()),
                UtilsApp.toBase64(usuarioVerificado.getStatusUsuarioCodigo()),
                UtilsApp.toBase64(Convert.formatDate(Convert.toDate(usuarioVerificado.getDataCadastro(),Parameters.STANDARD_DATE_FORMAT_RETURN_API, Locale.ENGLISH), Parameters.STANDARD_DATE_FORMAT_SIMPLE)),
                UtilsApp.toBase64(Convert.formatDate(Convert.toDate(usuarioVerificado.getDataDesativado(),Parameters.STANDARD_DATE_FORMAT_RETURN_API, Locale.ENGLISH), Parameters.STANDARD_DATE_FORMAT_SIMPLE)),
                UtilsApp.toBase64(usuarioVerificado.getEnderecoRua()),
                UtilsApp.toBase64(usuarioVerificado.getEnderecoNumero()), UtilsApp.toBase64(usuarioVerificado.getEnderecoBairro()),
                UtilsApp.toBase64(usuarioVerificado.getEnderecoComplemento()), UtilsApp.toBase64(usuarioVerificado.getEmail()),
                UtilsApp.toBase64(usuarioVerificado.getCep())
        );

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();

                if(apiResponse.getStatus().equals("OK")){
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "senha_alterada_sucesso");
                    UtilsApp.sleep(500);
                    UtilsApp.closeActivity(EsqueceuSenhaAlterar.this);
                    UtilsApp.goToActivity(getApplicationContext(), Login.class);
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_trocar_senha");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_trocar_senha");
            }
        });
    }

}
