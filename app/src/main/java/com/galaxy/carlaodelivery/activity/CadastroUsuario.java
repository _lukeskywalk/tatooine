package com.galaxy.carlaodelivery.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.crypt.MD5;
import com.galaxy.korriban.util.Convert;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CadastroUsuario extends AppCompatActivity {
    private EditText nomeEdtTxt;
    private EditText usuarioEdtTxt;
    private EditText emailEdtTxt;
    private EditText senhaEdtTxt;
    private EditText senhaNovamenteEdtTxt;
    private EditText ruaEdtTxt;
    private EditText numeroEdtTxt;
    private EditText bairroEdtTxt;
    private EditText complementoEdtTxt;
    private EditText cepEdtTxt;
    private Button btnOk;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);
        nomeEdtTxt = (EditText) findViewById(R.id.nomeEdTxtAtCadastroUsuario);
        usuarioEdtTxt = (EditText) findViewById(R.id.usuarioEdTxtAtCadastroUsuario);
        emailEdtTxt = (EditText) findViewById(R.id.emailEdTxtAtCadastroUsuario);
        senhaEdtTxt = (EditText) findViewById(R.id.senhaEdTxtAtCadastroUsuario);
        senhaNovamenteEdtTxt = (EditText) findViewById(R.id.senhaNovamenteEdTxtAtCadastroUsuario);
        ruaEdtTxt = (EditText) findViewById(R.id.ruaEdTxtAtCadastroUsuario);
        numeroEdtTxt = (EditText) findViewById(R.id.numeroEdTxtAtCadastroUsuario);
        bairroEdtTxt = (EditText) findViewById(R.id.bairroEdTxtAtCadastroUsuario);
        complementoEdtTxt = (EditText) findViewById(R.id.complementoEdTxtAtCadastroUsuario);
        cepEdtTxt = (EditText) findViewById(R.id.cepEdTxtAtCadastroUsuario);
        btnOk = (Button) findViewById(R.id.btnOkAtCadastro);


        UtilsApp.abrirConexao();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nome = nomeEdtTxt.getText().toString();
                String usuario = usuarioEdtTxt.getText().toString();
                String email = emailEdtTxt.getText().toString();
                String senha = senhaEdtTxt.getText().toString();
                String senhaNovamente = senhaNovamenteEdtTxt.getText().toString();
                String rua = ruaEdtTxt.getText().toString();
                String numero = numeroEdtTxt.getText().toString();
                String bairro = bairroEdtTxt.getText().toString();
                String complemento = complementoEdtTxt.getText().toString();
                String cep = cepEdtTxt.getText().toString();
                final String fields[] = {nome, usuario, email, senha,
                        senhaNovamente, rua, numero, bairro, cep};

                if(UtilsApp.checkStringValuesFilled(fields)){
                    if(UtilsApp.isValidEmail(email)){
                        if(senha.equals(senhaNovamente)){
                            if(senha.length() >= Parameters.MINIMUM_SIZE_PASSWORD && senhaNovamente.length() >= Parameters.MINIMUM_SIZE_PASSWORD){
                                if(UtilsApp.isValidCep(cep)){
                                    Usuario novoUsuario = new Usuario();
                                    novoUsuario.setNome(nome);
                                    novoUsuario.setUsuario(usuario);
                                    novoUsuario.setEmail(email);
                                    novoUsuario.setSenha(senha);
                                    novoUsuario.setDataCadastro(Convert.formatDate(Convert.toDate(new Date(),Parameters.STANDARD_DATE_FORMAT_RETURN_API, Locale.ENGLISH), Parameters.STANDARD_DATE_FORMAT_SIMPLE));
                                    novoUsuario.setEnderecoRua(rua);
                                    novoUsuario.setEnderecoNumero(numero);
                                    novoUsuario.setEnderecoBairro(bairro);
                                    novoUsuario.setEnderecoComplemento(complemento);
                                    novoUsuario.setCep(cep);
                                    criarUsuario(novoUsuario);
                                }else{
                                    UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_cep_valido");
                                }
                            }else{
                                UtilsApp.shortToastFromStrings(getApplicationContext(), "senha_deve_ser_maior_seis_caracteres");
                            }
                        }else{
                            UtilsApp.shortToastFromStrings(getApplicationContext(), "senhas_nao_coincidem");
                        }
                    }else{
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_email_valido");
                    }
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_todos_campos");
                }
            }
        });
    }

    private void criarUsuario(Usuario usuarioACriar){
        final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<APIResponse> call = apiInterface.postUsuario(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(usuarioACriar.getUsuario()),
                UtilsApp.toBase64(usuarioACriar.getNome()),
                UtilsApp.toBase64(MD5.toMD5(usuarioACriar.getSenha())),
                UtilsApp.toBase64(""),
                UtilsApp.toBase64(1),
                UtilsApp.toBase64(usuarioACriar.getDataCadastro()),
                UtilsApp.toBase64(""),
                UtilsApp.toBase64(usuarioACriar.getEnderecoRua()),
                UtilsApp.toBase64(usuarioACriar.getEnderecoNumero()),
                UtilsApp.toBase64(usuarioACriar.getEnderecoBairro()),
                UtilsApp.toBase64(usuarioACriar.getEnderecoComplemento()),
                UtilsApp.toBase64(usuarioACriar.getEmail()),
                UtilsApp.toBase64(usuarioACriar.getCep())
        );
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                String statusCriacao = apiResponse.getStatus();

                switch(statusCriacao){
                    case "OK":

                        Usuario usuarioCriado = apiResponse.getUsuario();

                        Call<APIResponse> callGrupoUsuario = apiInterface.postGrupoUsuario(
                                UtilsApp.generateValidTokenEndorApi(),
                                UtilsApp.toBase64(3),
                                UtilsApp.toBase64(usuarioCriado.getCodigo())
                        );
                        callGrupoUsuario.enqueue(new Callback<APIResponse>() {
                            @Override
                            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                                UtilsApp.shortToastFromStrings(getApplicationContext(), "usuario_criado_sucesso");
                                UtilsApp.sleep(1000);
                                UtilsApp.closeActivity(CadastroUsuario.this);
                            }

                            @Override
                            public void onFailure(Call<APIResponse> call, Throwable t) {
                                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_criar_usuario");
                            }
                        });
                        break;
                    case "erro_duplicado":
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "usuario_email_existem");
                    default:
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_criar_usuario");
                        break;
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_criar_usuario");
            }
        });
    }

}
