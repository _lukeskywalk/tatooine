package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.util.Convert;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeusDados extends BaseActivity{
    private TextView nomeUsuarioTxtVw;
    private TextView ruaUsuarioTxtVw;
    private TextView numeroUsuarioTxtVw;
    private TextView bairroUsuarioTxtVw;
    private TextView complementoUsuarioTxtVw;
    private TextView cepUsuarioTxtVw;
    private Button btnAtualizar;
    private Button btnAlterarSenha;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_meus_dados,null, false);
        contentFrame.addView(cv);

        nomeUsuarioTxtVw = (TextView) findViewById(R.id.nomeUsuarioTxtVwMeusDados);
        ruaUsuarioTxtVw = (TextView) findViewById(R.id.ruaUsuarioTxtVwMeusDados);
        numeroUsuarioTxtVw = (TextView) findViewById(R.id.numeroUsuarioTxtVwMeusDados);
        bairroUsuarioTxtVw = (TextView) findViewById(R.id.bairroUsuarioTxtVwMeusDados);
        complementoUsuarioTxtVw = (TextView) findViewById(R.id.complementoUsuarioTxtVwMeusDados);
        cepUsuarioTxtVw = (TextView) findViewById(R.id.cepUsuarioTxtVwMeusDados);
        btnAtualizar = (Button) findViewById(R.id.btnAtualizarDadosAtMeusDados);
        btnAlterarSenha = (Button) findViewById(R.id.btnAlterarSenhaAtMeusDados);

        btnAtualizar.setEnabled(false);
        btnAlterarSenha.setEnabled(false);

        btnAtualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AtualizarDadosCadastrais.class);
                intent.putExtra("usuario", usuario);

                UtilsApp.closeActivity(MeusDados.this);
                UtilsApp.goToActivity(getApplicationContext(), intent);
            }
        });

        btnAlterarSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlterarSenha.class);
                intent.putExtra("usuario", usuario);

                UtilsApp.closeActivity(MeusDados.this);
                UtilsApp.goToActivity(getApplicationContext(), intent);

            }
        });

        carregarDadosCadastrais();
    }

    private void carregarDadosCadastrais(){
        UtilsApp.abrirConexao();
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getUsuario(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(UtilsApp.getSavedInt(getApplicationContext(), Parameters.SHARED_PREF_CODIGO_USUARIO_LOGADO))
        );

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();

                if(apiResponse.getStatus().equals("OK")){
                    btnAtualizar.setEnabled(true);
                    btnAlterarSenha.setEnabled(true);
                    usuario = apiResponse.getUsuario();
                    nomeUsuarioTxtVw.setText(usuario.getNome());
                    ruaUsuarioTxtVw.setText(usuario.getEnderecoRua());
                    numeroUsuarioTxtVw.setText(usuario.getEnderecoNumero());
                    bairroUsuarioTxtVw.setText(usuario.getEnderecoBairro());
                    complementoUsuarioTxtVw.setText(usuario.getEnderecoComplemento());
                    cepUsuarioTxtVw.setText(usuario.getCep());
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_dados_usuario");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_dados_usuario");
            }
        });
    }
}
