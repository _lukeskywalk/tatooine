package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.list.adapter.PedidoAdapter;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Pedido;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeusPedidos extends BaseActivity{

    private ListView pedidosLtVw;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_meus_pedidos,null, false);
        contentFrame.addView(cv);

        pedidosLtVw = (ListView) findViewById(R.id.pedidosLtVwAtMeusPed);

        UtilsApp.abrirConexao();
        carregarPedidosUsuario();

    }

    public void carregarPedidosUsuario(){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getPedidosByUsuarioCodigo(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(UtilsApp.getSavedInt(getApplicationContext(), Parameters.SHARED_PREF_CODIGO_USUARIO_LOGADO))
        );

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                if(apiResponse.getStatus().equals("OK")){
                    final List<Pedido> listPedido = apiResponse.getListPedido();
                    PedidoAdapter pedidoAdapter = new PedidoAdapter(listPedido, MeusPedidos.this);
                    pedidosLtVw.setAdapter(pedidoAdapter);
                    pedidosLtVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Intent intent = new Intent(getApplicationContext(), PedidoActivity.class);
                            intent.putExtra("pedido", listPedido.get(i));
                            UtilsApp.closeActivity(MeusPedidos.this);
                            UtilsApp.goToActivity(getApplicationContext(), intent);
                        }
                    });
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_pedidos");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_pedidos");
            }
        });
    }
}
