package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.airbnb.deeplinkdispatch.DeepLink;
import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.StatusUsuario;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.UtilsApp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EsqueceuSenha extends BaseActivity {

    private EditText emailEdtTxt;
    private Button btnOk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_esqueceu_senha, null, false
        );
        contentFrame.addView(cv);

        emailEdtTxt = (EditText) findViewById(R.id.emailEdTxtAtEsqSenha);
        btnOk = (Button) findViewById(R.id.btnOkAtEsqueceuSenha);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailEsqueceuSenha = emailEdtTxt.getText().toString();
                if(UtilsApp.checkStringValuesFilled(emailEsqueceuSenha)){
                    if(UtilsApp.isValidEmail(emailEsqueceuSenha)){
                        UtilsApp.abrirConexao();
                        checkIfUserExistsAndSendMail(emailEsqueceuSenha);
                        //UtilsApp.sendEmailForgetPassword(EsqueceuSenha.this, emailEsqueceuSenha);
                    }else{
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_email_valido");
                    }

                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_todos_campos");
                }
            }
        });


    }

    private void checkIfUserExistsAndSendMail(final String email){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getUsuarioByEmail(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(email));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                if(apiResponse != null){
                    Usuario usuario = apiResponse.getUsuario();
                    if(usuario != null){
                        StatusUsuario statusUsuario = usuario.getStatusUsuario();
                        if(statusUsuario != null){
                            if(statusUsuario.getNome().equals(UtilsApp.getString(getApplicationContext(), "ativo"))){

                                UtilsApp.sendEmailForgetPassword(EsqueceuSenha.this, email, usuario.getCodigo());

                            }else{
                                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_enviar_email_esqueci_senha");
                            }
                        }else{
                            UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_enviar_email_esqueci_senha");
                        }
                    }else{
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_enviar_email_esqueci_senha");
                    }
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_enviar_email_esqueci_senha");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_enviar_email_esqueci_senha");
            }
        });
    }
}
