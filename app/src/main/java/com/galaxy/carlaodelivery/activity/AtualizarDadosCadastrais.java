package com.galaxy.carlaodelivery.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.util.Convert;

import java.util.Date;
import java.util.Locale;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AtualizarDadosCadastrais extends AppCompatActivity {

    private EditText nomeUsuarioEdtTxt;
    private EditText ruaUsuarioEdtTxt;
    private EditText numeroUsuarioEdtTxt;
    private EditText bairroUsuarioEdtTxt;
    private EditText complementoUsuarioEdtTxt;
    private EditText cepUsuarioEdtTxt;
    private Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atualizar_dados_cadastrais);

        nomeUsuarioEdtTxt = (EditText) findViewById(R.id.nomeUsuarioEdtTxtAtualizarDadosCadastrais);
        ruaUsuarioEdtTxt = (EditText) findViewById(R.id.ruaUsuarioEdtTxtAtualizarDadosCadastrais);
        numeroUsuarioEdtTxt = (EditText) findViewById(R.id.numeroUsuarioEdtTxtAtualizarDadosCadastrais);
        bairroUsuarioEdtTxt = (EditText) findViewById(R.id.bairroUsuarioEdtTxtAtualizarDadosCadastrais);
        complementoUsuarioEdtTxt = (EditText) findViewById(R.id.complementoUsuarioEdtTxtAtualizarDadosCadastrais);
        cepUsuarioEdtTxt = (EditText) findViewById(R.id.cepUsuarioEdtTxtAtualizarDadosCadastrais);
        btnOk = (Button) findViewById(R.id.btnOkAtualizarDadosCadastrais);

        Intent intent = getIntent();
        final Usuario usuario = (Usuario)intent.getSerializableExtra("usuario");
        nomeUsuarioEdtTxt.setText(usuario.getNome());
        ruaUsuarioEdtTxt.setText(usuario.getEnderecoRua());
        numeroUsuarioEdtTxt.setText(usuario.getEnderecoNumero());
        bairroUsuarioEdtTxt.setText(usuario.getEnderecoBairro());
        complementoUsuarioEdtTxt.setText(usuario.getEnderecoComplemento());
        cepUsuarioEdtTxt.setText(usuario.getCep());


        UtilsApp.abrirConexao();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario.setNome(nomeUsuarioEdtTxt.getText().toString());
                usuario.setEnderecoRua(ruaUsuarioEdtTxt.getText().toString());
                usuario.setEnderecoNumero(numeroUsuarioEdtTxt.getText().toString());
                usuario.setEnderecoBairro(bairroUsuarioEdtTxt.getText().toString());
                usuario.setEnderecoComplemento(complementoUsuarioEdtTxt.getText().toString());
                usuario.setCep(cepUsuarioEdtTxt.getText().toString());

                atualizarUsuario(usuario);
            }
        });
    }

    private void atualizarUsuario(Usuario usuarioAtualizar){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.putUsuario(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(usuarioAtualizar.getCodigo()),
                UtilsApp.toBase64(usuarioAtualizar.getNome()),
                UtilsApp.toBase64(usuarioAtualizar.getUsuario()),
                UtilsApp.toBase64(usuarioAtualizar.getSenha()),
                UtilsApp.toBase64(usuarioAtualizar.getUltimaSenha()),
                UtilsApp.toBase64(usuarioAtualizar.getStatusUsuarioCodigo()),
                UtilsApp.toBase64(UtilsApp.getDateWithLocale(usuarioAtualizar.getDataCadastro(), Parameters.STANDARD_DATE_FORMAT_SIMPLE)),
                UtilsApp.toBase64(UtilsApp.getDateWithLocale(usuarioAtualizar.getDataDesativado(), Parameters.STANDARD_DATE_FORMAT_SIMPLE)),
                UtilsApp.toBase64(usuarioAtualizar.getEnderecoRua()),
                UtilsApp.toBase64(usuarioAtualizar.getEnderecoNumero()),
                UtilsApp.toBase64(usuarioAtualizar.getEnderecoBairro()),
                UtilsApp.toBase64(usuarioAtualizar.getEnderecoComplemento()),
                UtilsApp.toBase64(usuarioAtualizar.getEmail()),
                UtilsApp.toBase64(usuarioAtualizar.getCep())
        );
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                if(apiResponse.getStatus().equals("OK")){
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "dados_atualizados_sucesso");
                    UtilsApp.sleep(500);
                    UtilsApp.closeActivity(AtualizarDadosCadastrais.this);
                    UtilsApp.goToActivity(getApplicationContext(), MeusDados.class);
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_atualizar_dados_usuario");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_atualizar_dados_usuario");
            }
        });
    }
}
