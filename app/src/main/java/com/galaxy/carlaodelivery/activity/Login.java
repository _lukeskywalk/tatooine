package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.Produto;
import com.galaxy.carlaodelivery.model.StatusPedido;
import com.galaxy.carlaodelivery.model.StatusUsuario;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.crypt.Base;
import com.galaxy.korriban.crypt.MD5;
import com.galaxy.korriban.util.Convert;


import java.util.Date;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    private EditText usuarioEdtTxt;
    private EditText senhaEdtTxt;
    private Button btnOk;
    private TextView cadastreSeTxtVw;
    private TextView esqueciSenhaTxtVw;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        abrirActivityHome();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usuarioEdtTxt = (EditText) findViewById(R.id.usuarioEdTxtAtLogin);
        senhaEdtTxt = (EditText) findViewById(R.id.senhaEdTxtAtLogin);
        btnOk = (Button) findViewById(R.id.btnOkAtLogin);
        cadastreSeTxtVw = (TextView) findViewById(R.id.cadastreseTxtVwAtLogin);
        esqueciSenhaTxtVw = (TextView) findViewById(R.id.esqueciSenhaTxtVwAtLogin);

        /*define variáveis importantes*/
        UtilsApp.abrirConexao();
        preCarregarValoresPadrao();


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String usuario = usuarioEdtTxt.getText().toString();
                String senha = senhaEdtTxt.getText().toString();
                UtilsApp.abrirConexao();
                if (UtilsApp.checkStringValuesFilled(usuario, senha)) {
                    loginUsuario(usuario, MD5.toMD5(senha));
                } else {
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "preencha_todos_campos");
                }
            }
        });

        cadastreSeTxtVw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilsApp.goToActivity(getApplicationContext(), CadastroUsuario.class);
            }
        });

        esqueciSenhaTxtVw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilsApp.goToActivity(getApplicationContext(), EsqueceuSenha.class);
            }
        });
    }



    private void loginUsuario(final String usuarioStr, final String senhaStr) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getUsuario(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(usuarioStr));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                if (apiResponse != null) {
                    Usuario usuario = apiResponse.getUsuario();
                    if (usuario != null) {
                        StatusUsuario statusUsuario = usuario.getStatusUsuario();
                        if (statusUsuario != null) {
                            if (statusUsuario.getNome().equals(UtilsApp.getString(getApplicationContext(), "ativo"))) {
                                if (usuario.getSenha().equals(senhaStr)) {
                                    UtilsApp.saveInt(getApplicationContext(), Parameters.SHARED_PREF_CODIGO_USUARIO_LOGADO, usuario.getCodigo());
                                    UtilsApp.saveString(getApplicationContext(), Parameters.SHARED_PREF_USUARIO_LOGADO, usuarioStr);
                                    UtilsApp.saveString(getApplicationContext(), Parameters.SHARED_PREF_EMAIL_USUARIO_LOGADO, usuario.getEmail());

                                    Intent intentHome = new Intent(Login.this, Home.class);
                                    startActivity(intentHome);
                                } else {
                                    UtilsApp.shortToastFromStrings(getApplicationContext(), "usuario_senha_invalidos");
                                }
                            } else {
                                UtilsApp.shortToastFromStrings(getApplicationContext(), "nao_foi_possivel_encontrar_usuario");
                            }
                        } else {
                            UtilsApp.shortToastFromStrings(getApplicationContext(), "usuario_senha_invalidos");
                        }
                    } else {
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "usuario_senha_invalidos");
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                t.printStackTrace();
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_desconhecido");
            }
        });
    }

    private void preCarregarValoresPadrao(){
        /*load minutes duration token forgot password*/
        if(UtilsApp.getSavedInt(getApplicationContext(), Parameters.SHARED_PREF_MINUTES_DURATION_TOKEN_FORGOT_PASSWORD) <= 0){
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<APIResponse> call = apiInterface.getParametro(UtilsApp.generateValidTokenEndorApi(),
                    UtilsApp.toBase64(Parameters.MINUTES_DURATION_TOKEN_FORGOT_PASSWORD));
            call.enqueue(new Callback<APIResponse>() {
                @Override
                public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                    int minutesDurationTokenForgotPassword = Parameters.STANDARD_DURATION_TOKEN_FORGOT_PASSWORD;
                    if(response != null){
                        APIResponse apiResponse = response.body();
                        if(apiResponse.getParametro() != null){
                            minutesDurationTokenForgotPassword = Convert.toInt(apiResponse.getParametro().getValor());
                        }
                    }
                    UtilsApp.saveInt(getApplicationContext(), Parameters.SHARED_PREF_MINUTES_DURATION_TOKEN_FORGOT_PASSWORD, minutesDurationTokenForgotPassword);

                }

                @Override
                public void onFailure(Call<APIResponse> call, Throwable t) {
                    UtilsApp.saveInt(getApplicationContext(), Parameters.SHARED_PREF_MINUTES_DURATION_TOKEN_FORGOT_PASSWORD, Parameters.STANDARD_DURATION_TOKEN_FORGOT_PASSWORD);
                }
            });
        }


        /*load user permissions*/
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getPermissoesUsuario(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(UtilsApp.getSavedInt(getApplicationContext(), Parameters.SHARED_PREF_CODIGO_USUARIO_LOGADO))
        );
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                System.out.println(apiResponse);
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                Log.d("ERROR USERPERMISSIONS", t.getMessage());
            }
        });

        /*load codigo primeiro status pedido*/
        APIInterface apiInterface1 = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> callStatusPedido = apiInterface1.getStatusPedidoByNome(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64("Em Fila")
        );
        callStatusPedido.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();

                if(apiResponse.getStatus().equals("OK")){
                    StatusPedido statusPedido = apiResponse.getStatusPedido();
                    UtilsApp.saveInt(getApplicationContext(), Parameters.SHARED_PREF_INITIAL_STATUS_PEDIDO, statusPedido.getCodigo());
                }else{
                    UtilsApp.saveInt(getApplicationContext(), Parameters.SHARED_PREF_INITIAL_STATUS_PEDIDO, Parameters.VALUE_INITIAL_STATUS_PEDIDO);
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.saveInt(getApplicationContext(), Parameters.SHARED_PREF_INITIAL_STATUS_PEDIDO, Parameters.VALUE_INITIAL_STATUS_PEDIDO);
            }
        });
    }

    public void abrirActivityHome(){
        String usuarioLogado = UtilsApp.getSavedString(getApplicationContext(), Parameters.SHARED_PREF_USUARIO_LOGADO);
        if(usuarioLogado != null){
            if(!usuarioLogado.isEmpty()){
                UtilsApp.goToActivity(getApplicationContext(),Home.class);
            }
        }
    }

    public void testeGetStatusUsuario() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getStatusUsuario(UtilsApp.generateValidTokenEndorApi(), UtilsApp.toBase64(1));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();

                UtilsApp.shortToast(getApplicationContext(), apiResponse.getStatusUsuario().getNome());
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testePostStatusUsuario() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.postStatusUsuario(UtilsApp.generateValidTokenEndorApi(), UtilsApp.toBase64("grupinho teste"));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                Toast.makeText(getApplicationContext(), apiResponse.getStatus(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testePutStatusUsuario() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.putStatusUsuario(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(3), UtilsApp.toBase64("mudou"));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                Toast.makeText(getApplicationContext(), apiResponse.getStatus(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testeDeleteStatusUsuario() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.deleteStatusUsuario(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(3));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                Toast.makeText(getApplicationContext(), apiResponse.getStatus(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });


    }

    public void testePostUsuario() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.postUsuario(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64("novodonovousuario"), UtilsApp.toBase64("novonomeusuario"),
                UtilsApp.toBase64(MD5.toMD5("123456")), UtilsApp.toBase64(""),
                UtilsApp.toBase64(6), UtilsApp.toBase64("23/10/1998"),
                UtilsApp.toBase64(""), UtilsApp.toBase64("ruadousuario"),
                UtilsApp.toBase64(123), UtilsApp.toBase64("bairrousuario"),
                UtilsApp.toBase64("complementousuario"), UtilsApp.toBase64("emailemail@email.com"),
                UtilsApp.toBase64("07243123"));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                Toast.makeText(getApplicationContext(), apiResponse.getStatus(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testePutUsuario() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.putUsuario(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(24), UtilsApp.toBase64("teste"),
                UtilsApp.toBase64("teste2"), UtilsApp.toBase64(MD5.toMD5("jaldjaslakdsnm,c")),
                UtilsApp.toBase64(""), UtilsApp.toBase64(4), UtilsApp.toBase64("12/05/2018"),
                UtilsApp.toBase64(""), UtilsApp.toBase64("enre"), UtilsApp.toBase64(12),
                UtilsApp.toBase64("ted"), UtilsApp.toBase64("dsal"), UtilsApp.toBase64("tes#lds@dsl.com"),
                UtilsApp.toBase64("07568-000"));

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                Toast.makeText(getApplicationContext(), apiResponse.getStatus(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testeDeleteUsuario() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.deleteUsuario(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(24));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                Toast.makeText(getApplicationContext(), apiResponse.getStatus(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testeGetStatusPedido() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getStatusPedido(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(9));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                StatusPedido statusPedido = apiResponse.getStatusPedido();
                Toast.makeText(getApplicationContext(), statusPedido.getNome(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testePostStatusPedido() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.postStatusPedido(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64("testes"));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                Toast.makeText(getApplicationContext(), apiResponse.getStatus(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testePutStatusPedido() {
        final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.putStatusPedido(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(10), UtilsApp.toBase64("testemudanca"));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                UtilsApp.shortToast(getApplicationContext(), apiResponse.getStatus());
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testeDeleteStatusPedido() {
        final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.deleteStatusPedido(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(10));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                UtilsApp.shortToast(getApplicationContext(), apiResponse.getStatus());
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }

    public void testeGetProduto() {
        final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getProduto(UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(5));
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse produto = response.body();
                UtilsApp.shortToast(getApplicationContext(), produto.getStatus());
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {

            }
        });
    }


}
