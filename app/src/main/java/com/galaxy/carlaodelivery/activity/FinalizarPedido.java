package com.galaxy.carlaodelivery.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.list.adapter.FormaPagamentoAdapter;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.FormaPagamento;
import com.galaxy.carlaodelivery.model.Parametro;
import com.galaxy.carlaodelivery.model.Pedido;
import com.galaxy.carlaodelivery.model.PedidoProduto;
import com.galaxy.carlaodelivery.model.Produto;
import com.galaxy.carlaodelivery.model.Usuario;
import com.galaxy.carlaodelivery.utils.CarrinhoComprasSingleton;
import com.galaxy.carlaodelivery.utils.Parameters;
import com.galaxy.carlaodelivery.utils.UtilsApp;
import com.galaxy.korriban.util.Convert;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinalizarPedido extends BaseActivity{

    private TextView valorTotalProdutosTxtVw;
    private TextView valorTotalEntregaTxtVw;
    private TextView valorTotalPedidoTxtVw;
    private Spinner formaPagamentoSpnr;
    private Button btnVoltarDadosEndereco;
    private Button btnConfirmarPedidoFim;
    private BigDecimal valorFinalPedido;
    private Integer formaPagamentoSelecionada;
    private String rua;
    private String numero;
    private String bairro;
    private String complemento;
    private String cep;
    Usuario usuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View cv = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.activity_finalizar_pedido,null, false);
        contentFrame.addView(cv);

        valorTotalProdutosTxtVw = (TextView) findViewById(R.id.valorTotalProdutosTxtVwAtFinPed);
        valorTotalEntregaTxtVw = (TextView) findViewById(R.id.valorTotalEntregaTxtVwAtFinPed);
        valorTotalPedidoTxtVw = (TextView) findViewById(R.id.valorTotalPedidoTxtVwAtFinPed);
        formaPagamentoSpnr = (Spinner) findViewById(R.id.formaPagamentoSpnrAtFinPed);
        btnVoltarDadosEndereco = (Button) findViewById(R.id.btnVoltarDadosEnderecoAtFinPed);
        btnConfirmarPedidoFim = (Button) findViewById(R.id.btnConfirmarPedidoFimAtFinPed);

        btnConfirmarPedidoFim.setEnabled(false);
        carregarValorTaxaEntregaEValorTotal();

        valorTotalProdutosTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces(calcularSubTotalCompra())));


        carregarSpinnerFormaPagamento();

        final Intent intent = getIntent();
        usuario = (Usuario) intent.getSerializableExtra("usuario");
        rua = intent.getStringExtra("rua");
        numero = intent.getStringExtra("numero");
        bairro = intent.getStringExtra("bairro");
        complemento = intent.getStringExtra("complemento");
        cep = intent.getStringExtra("cep");

        btnVoltarDadosEndereco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplicationContext(), FinalizarDadosEnderecoPedido.class);
                intent1.putExtra("usuario", usuario);
                UtilsApp.closeActivity(FinalizarPedido.this);
                UtilsApp.goToActivity(getApplicationContext(), intent1);
            }
        });

        btnConfirmarPedidoFim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalizarPedido();
            }
        });



    }

    private void carregarSpinnerFormaPagamento(){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getFormaPagamentos(UtilsApp.generateValidTokenEndorApi());
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();

                if(apiResponse.getStatus().equals("OK")){
                    FormaPagamento formaPagamentoZero = new FormaPagamento();
                    formaPagamentoZero.setCodigo(0);
                    formaPagamentoZero.setNome(UtilsApp.getString(getApplicationContext(), "selecione"));
                    List<FormaPagamento> listFormaPagamento = new LinkedList<FormaPagamento>();
                    listFormaPagamento.add(formaPagamentoZero);
                    listFormaPagamento.addAll(apiResponse.getListFormaPagamento());

                    final FormaPagamentoAdapter formaPagamentoAdapter = new FormaPagamentoAdapter(
                            FinalizarPedido.this,
                            android.R.layout.simple_spinner_item,
                            listFormaPagamento
                    );
                    formaPagamentoSpnr.setAdapter(formaPagamentoAdapter);
                    formaPagamentoSpnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            FormaPagamento formaPagamento = formaPagamentoAdapter.getItem(position);
                            formaPagamentoSelecionada = formaPagamento.getCodigo();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    btnConfirmarPedidoFim.setEnabled(true);
                }else{
                    UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_forma_pagamentos");
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_carregar_forma_pagamentos");
            }
        });

    }

    private BigDecimal calcularSubTotalCompra(){
        BigDecimal subTotal = new BigDecimal(0);
        for(Map<String, Object> map : CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao){
            Produto produto = (Produto) map.get("produto");
            Integer quantidade = Convert.getInt(map,"quantidade");
            BigDecimal valorQuantidadeProduto = produto.getPreco().multiply(new BigDecimal(quantidade));
            subTotal = subTotal.add(valorQuantidadeProduto);
        }

        return subTotal;
    }

    private void carregarValorTaxaEntregaEValorTotal(){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getParametro(
                UtilsApp.generateValidTokenEndorApi(),
                UtilsApp.toBase64(Parameters.DELIVERY_TAX)
        );
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                if(apiResponse.getStatus().equals("OK")){
                    Parametro parametro = apiResponse.getParametro();
                    valorTotalEntregaTxtVw.setText(String.format("R$%s",UtilsApp.stringTwoDecimalPlaces(new BigDecimal(parametro.getValor()))));
                    valorTotalPedidoTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces  (calcularSubTotalCompra().add(new BigDecimal(parametro.getValor())))));
                    valorFinalPedido = calcularSubTotalCompra().add(new BigDecimal(parametro.getValor()));
                }else{
                    valorTotalEntregaTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces(Parameters.STANDARD_VALUE_DELIVERY_TAX)));
                    valorTotalPedidoTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces  (calcularSubTotalCompra().add(Parameters.STANDARD_VALUE_DELIVERY_TAX))));
                    valorFinalPedido = calcularSubTotalCompra().add(Parameters.STANDARD_VALUE_DELIVERY_TAX);
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                valorTotalEntregaTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces(Parameters.STANDARD_VALUE_DELIVERY_TAX)));
                valorTotalPedidoTxtVw.setText(String.format("R$%s", UtilsApp.stringTwoDecimalPlaces  (calcularSubTotalCompra().add(Parameters.STANDARD_VALUE_DELIVERY_TAX))));
                valorFinalPedido = calcularSubTotalCompra().add(Parameters.STANDARD_VALUE_DELIVERY_TAX);
            }
        });
    }

    private void finalizarPedido(){
        if(formaPagamentoSelecionada != null){
            if(formaPagamentoSelecionada > 0){
                Pedido pedido = new Pedido();
                pedido.setCriadoEm(Convert.formatDate(Convert.toDate(new Date(),Parameters.STANDARD_DATE_FORMAT_RETURN_API, Locale.ENGLISH), Parameters.STANDARD_DATE_FORMAT_SIMPLE));
                String enderecoEntrega = String.format(
                        UtilsApp.getString(getApplicationContext(), "rua") + " %s " +
                        UtilsApp.getString(getApplicationContext(), "numero") + " %s " +
                        UtilsApp.getString(getApplicationContext(), "bairro") + " %s " +
                        UtilsApp.getString(getApplicationContext(), "complemento") + " %s " +
                        UtilsApp.getString(getApplicationContext(), "cep") + " %s ", rua, numero, bairro, complemento, cep);
                pedido.setEnderecoEntrega(enderecoEntrega);
                pedido.setModificadoEm(Convert.formatDate(Convert.toDate(new Date(),Parameters.STANDARD_DATE_FORMAT_RETURN_API, Locale.ENGLISH), Parameters.STANDARD_DATE_FORMAT_SIMPLE));
                pedido.setFormaPagamentoCodigo(formaPagamentoSelecionada);
                pedido.setStatusPedidoCodigo(UtilsApp.getSavedInt(getApplicationContext(), Parameters.SHARED_PREF_INITIAL_STATUS_PEDIDO));
                pedido.setUsuarioCodigo(usuario.getCodigo());
                pedido.setValor(valorFinalPedido);

                final APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                Call<APIResponse> callPedido = apiInterface.postPedido(
                        UtilsApp.generateValidTokenEndorApi(),
                        UtilsApp.toBase64(pedido.getUsuarioCodigo()),
                        UtilsApp.toBase64(pedido.getStatusPedidoCodigo()),
                        UtilsApp.toBase64(Convert.toString(pedido.getValor())),
                        UtilsApp.toBase64(pedido.getCriadoEm()),
                        UtilsApp.toBase64(pedido.getModificadoEm()),
                        UtilsApp.toBase64(pedido.getEnderecoEntrega()),
                        UtilsApp.toBase64(pedido.getFormaPagamentoCodigo())
                );

                callPedido.enqueue(new Callback<APIResponse>() {
                    @Override
                    public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                        APIResponse apiResponse = response.body();
                        if(apiResponse.getStatus().equals("OK")){
                            Pedido pedidoCriado = apiResponse.getPedido();
                            List<PedidoProduto> listToSave = new LinkedList<>();

                            for(int i = 0; i < CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao.size(); i++){
                                Map<String, Object> item = CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao.get(i);
                                Produto produto = (Produto) item.get("produto");
                                Integer quantidade = Integer.valueOf(String.valueOf(item.get("quantidade")));

                                produto.setEstoque(produto.getEstoque() - quantidade);


                                Call<APIResponse> callProduto = apiInterface.putProduto(
                                        UtilsApp.generateValidTokenEndorApi(),
                                        UtilsApp.toBase64(produto.getCodigo()),
                                        UtilsApp.toBase64(produto.getNome()),
                                        UtilsApp.toBase64(produto.getDescricao()),
                                        UtilsApp.toBase64(produto.getEstoque()),
                                        UtilsApp.toBase64(produto.getPreco().toString())
                                );

                                callProduto.enqueue(new Callback<APIResponse>() {
                                    @Override
                                    public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                                        System.out.println("ATUALIZADO");
                                    }

                                    @Override
                                    public void onFailure(Call<APIResponse> call, Throwable t) {
                                        System.out.println("ERRO");
                                    }
                                });


                                PedidoProduto pedidoProduto = new PedidoProduto();
                                pedidoProduto.setPedidoCodigo(pedidoCriado.getCodigo());
                                pedidoProduto.setProdutoCodigo(produto.getCodigo());
                                pedidoProduto.setQuantidade(quantidade);

                                Call<APIResponse> callPedidoProduto = apiInterface.postPedidoProduto(
                                        UtilsApp.generateValidTokenEndorApi(),
                                        UtilsApp.toBase64(pedidoProduto.getPedidoCodigo()),
                                        UtilsApp.toBase64(pedidoProduto.getProdutoCodigo()),
                                        UtilsApp.toBase64(pedidoProduto.getQuantidade())
                                );

                                final int finalI = i;
                                callPedidoProduto.enqueue(new Callback<APIResponse>() {
                                    @Override
                                    public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                                        APIResponse apiResponsePedidoProduto = response.body();

                                        if(apiResponsePedidoProduto.getStatus().equals("OK")){
                                            if(finalI == CarrinhoComprasSingleton.getInstance().carrinhoComprasSessao.size()-1){
                                                UtilsApp.shortToastFromStrings(getApplicationContext(), "pedido_efetuado_sucesso");
                                                UtilsApp.closeActivity(FinalizarPedido.this);
                                                UtilsApp.goToActivity(getApplicationContext(), MeusPedidos.class);
                                            }

                                        }else{
                                            UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_salvar_item_pedido");
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<APIResponse> call, Throwable t) {
                                        UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_salvar_item_pedido");
                                    }
                                });
                            }

                        }else{
                            UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_finalizar_pedido");
                        }
                    }

                    @Override
                    public void onFailure(Call<APIResponse> call, Throwable t) {
                        UtilsApp.shortToastFromStrings(getApplicationContext(), "erro_finalizar_pedido");
                    }
                });

            }else{
                UtilsApp.shortToast(getApplicationContext(),UtilsApp.getString(getApplicationContext(), "selecione_forma_pagamento"));
            }
        }else{
            UtilsApp.shortToast(getApplicationContext(),UtilsApp.getString(getApplicationContext(), "selecione_forma_pagamento"));
        }
    }

}
