package com.galaxy.carlaodelivery.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CarrinhoComprasSingleton {
    private static CarrinhoComprasSingleton mInstance = null;
    public List<Map<String, Object>> carrinhoComprasSessao = new LinkedList<>();

    protected CarrinhoComprasSingleton(){}

    public static synchronized CarrinhoComprasSingleton getInstance(){
        if(mInstance == null){
            mInstance = new CarrinhoComprasSingleton();
        }
        return mInstance;
    }

}
