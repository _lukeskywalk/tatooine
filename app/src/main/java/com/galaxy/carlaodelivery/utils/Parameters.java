package com.galaxy.carlaodelivery.utils;

import java.math.BigDecimal;

/**
 * Created by edilson on 26/03/18.
 */

public class Parameters {
    public static final String BASE_URL = "https://endor-api.herokuapp.com";
    //public static final String BASE_URL = "http://10.0.3.2:5000";
    public static final String STATUS_USUARIO_URL = "/statusUsuario/";
    public static final String USUARIO_URL = "/usuario/";
    public static final String STATUS_PEDIDO_URL = "/statusPedido/";
    public static final String PRODUTO_URL = "/produto/";
    public static final String PEDIDO_URL = "/pedido/";
    public static final String PEDIDO_PRODUTO_URL = "/pedidoProduto/";
    public static final String PARAMETRO_URL = "/parametro/";
    public static final String GRUPO_URL = "/grupo/";
    public static final String GRUPO_USUARIO_URL = "/grupoUsuario/";
    public static final String PERMISSAO_URL = "/permissao/";
    public static final String GRUPO_PERMISSAO_URL = "/grupoPermissao/";
    public static final String IMAGEM_PRODUTO_URL = "/imagemProduto/";


    public static final String TOKEN_API = "b3850f7b08a0d46b80905fd04ccbc2de";


    public static final String STANDARD_DATE_FORMAT_RETURN_API = "EEE, dd MMM yyyy HH:mm:ss z";
    public static final String STANDARD_DATE_FORMAT_SIMPLE = "dd/MM/yyyy";


    public static final String SHARED_PREF_CODIGO_USUARIO_LOGADO = "codigo_usuario";
    public static final String SHARED_PREF_USUARIO_LOGADO = "usuario_logado";
    public static final String SHARED_PREF_EMAIL_USUARIO_LOGADO = "email_usuario_logado";
    public static final String SHARED_PREF_MINUTES_DURATION_TOKEN_FORGOT_PASSWORD = "minutes_duration_token_forgot_password";
    public static final String SHARED_PREF_USER_PERMISSIONS = "user_permissions";
    public static final String SHARED_PREF_INITIAL_STATUS_PEDIDO = "initial status pedido";



    public static final String SENHA_EMAIL_CARLAO = "Tgdre4rDGRE43d";
    public static final String EMAIL_CARLAO = "carlaodelivery@gmail.com";

    public static final int STANDARD_DURATION_TOKEN_FORGOT_PASSWORD = 4000;
    public static final String MINUTES_DURATION_TOKEN_FORGOT_PASSWORD = "minutes_duration_token_forgot_password";
    public static final int MINIMUM_SIZE_PASSWORD = 6;
    public static final BigDecimal STANDARD_VALUE_DELIVERY_TAX = new BigDecimal(3);
    public static final String DELIVERY_TAX = "valor_entrega";
    public static final int VALUE_INITIAL_STATUS_PEDIDO = 1;

}
