package com.galaxy.carlaodelivery.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.galaxy.carlaodelivery.R;
import com.galaxy.carlaodelivery.activity.EsqueceuSenhaAlterar;
import com.galaxy.carlaodelivery.http.APIClient;
import com.galaxy.carlaodelivery.http.APIInterface;
import com.galaxy.carlaodelivery.mail.GMailSender;
import com.galaxy.carlaodelivery.model.APIResponse;
import com.galaxy.carlaodelivery.model.TokenUsuario;
import com.galaxy.carlaodelivery.thread.NotifyingThread;
import com.galaxy.carlaodelivery.thread.ThreadCompleteListener;
import com.galaxy.carlaodelivery.thread.ThreadMail;
import com.galaxy.korriban.crypt.Base;
import com.galaxy.korriban.util.Convert;
import com.galaxy.korriban.util.DateUtil;

import org.apache.commons.validator.routines.EmailValidator;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by edilson on 26/03/18.
 */

public class UtilsApp {
    public static String getString(Context context, String name) {
        String returnString = "";
        try {
            String packageName = context.getPackageName();
            int resourceId = context.getResources().getIdentifier(name, "string", packageName);
            returnString = context.getString(resourceId);
            if (returnString == null) {
                returnString = "";
            }
        } catch (Exception ex) {
            returnString = name;
        }
        return returnString;
    }

    public static void shortToastFromStrings(Context context, String text) {
        Toast.makeText(context, getString(context, text), Toast.LENGTH_SHORT).show();
    }

    public static void longToastFromStrings(Context context, String text) {
        Toast.makeText(context, getString(context, text), Toast.LENGTH_LONG).show();
    }

    public static void shortToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void longToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static boolean checkStringValuesFilled(String... values) {
        boolean filled = true;
        for (String value : values) {
            if (value != null) {
                if (!value.isEmpty()) {
                    filled = true;
                } else {
                    filled = false;
                    break;
                }
            } else {
                filled = false;
                break;
            }
        }
        return filled;
    }

    public static boolean isValidEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    public static boolean isValidCep(String cep) {
        //String cepPattern = "\\d{5}-\\d{3}";
        cep = cep.replaceAll("[^0-9]", "");
        String cepPattern = "\\d{8}";

        if (cep != null) {
            return cep.matches(cepPattern);
        }
        return false;
    }

    public static String toBase64(Integer number) {
        return Base64.encodeToString(Convert.toString(number).getBytes(), Base64.DEFAULT);
    }

    public static String toBase64(String text) {
        if(text != null) {
            return Base64.encodeToString(text.getBytes(), Base64.DEFAULT);
        }else{
            return "";
        }
    }

    public static String getDateWithLocale(String dateString, String format){
        try {
            Date date = Convert.toDate(dateString, Parameters.STANDARD_DATE_FORMAT_RETURN_API, Locale.ENGLISH);
            long milliDate = DateUtil.getMilliseconds(date);
            long milliDatePlusOneDay = milliDate + (1000 * 60 * 60 * 24);//one day is lost beacause of gmt in format, correct, nice and scum
            Date dateConverted = new Date(milliDatePlusOneDay);

            return Convert.formatDate(dateConverted, format);
        }catch (Exception ex){
            return null;
        }
    }

    public static Date getDateWithLocal(String dateString){
        try{
            Date date = Convert.toDate(dateString, Parameters.STANDARD_DATE_FORMAT_RETURN_API, Locale.ENGLISH);
            long milliDate = DateUtil.getMilliseconds(date);
            long milliDatePlusOneDay = milliDate + (1000 * 60 * 60 * 24);//one day is lost beacause of gmt in format, correct, nice and scum
            Date dateConverted = new Date(milliDatePlusOneDay);
            return dateConverted;
        }catch(Exception ex){
            return null;
        }
    }

    public static String fromBase64(String encoded64) {
        try {
            return new String(Base64.decode(encoded64, Base64.DEFAULT), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    public static String generateValidTokenEndorApi() {
        long timeNow = System.currentTimeMillis();
        Base base = new Base();
        return toBase64(Parameters.TOKEN_API + ";" + timeNow);
    }

    public static void exitApp(Activity activity, Class<?> activityClass) {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.getApplicationContext().startActivity(startMain);
        activity.finish();
        if (Build.VERSION.SDK_INT >= 16) {
            activity.finishAffinity();
        }
        System.exit(0);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void goToActivity(Context context, Class<?> destination) {
        Intent intent = new Intent(context, destination);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void goToActivity(Context context, Intent intent){
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void closeActivity(Activity activity){
        activity.finish();
    }

    public static void saveString(Context context, String key, String textToSave) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.key_shared_preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, textToSave);
        editor.apply();
    }

    public static void saveInt(Context context, String key, int intToSave){
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.key_shared_preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, intToSave);
        editor.apply();
    }

    public static String getSavedString(Context context, String textToGet) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.key_shared_preferences), Context.MODE_PRIVATE);
        return sharedPreferences.getString(textToGet, "");
    }

    public static int getSavedInt(Context context, String intToGet){
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.key_shared_preferences), Context.MODE_PRIVATE
        );
        return sharedPreferences.getInt(intToGet, 0);
    }

    public static void removeSavedString(Context context, String textToDelete) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.key_shared_preferences), Context.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(textToDelete);
        editor.apply();
    }

    public static void sendEmailForgetPassword(final Activity activity, final String recipient, final int userCode) {
        ThreadCompleteListener threadCompleteListener = new ThreadCompleteListener() {
            @Override
            public void notifyOfThreadComplete(Thread thread) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        UtilsApp.shortToastFromStrings(activity.getApplicationContext(),
                                getString(activity.getApplicationContext(), "email_enviado_sucesso"));
                    }
                });
            }
        };

        NotifyingThread threadMail = new ThreadMail();
        threadMail.setContext(activity.getApplicationContext());
        threadMail.setRecipient(recipient);
        threadMail.setUserCode(userCode);
        threadMail.addListener(threadCompleteListener);
        threadMail.start();
    }

    public static void abrirConexao() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<APIResponse> call = apiInterface.getInfo();
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                APIResponse apiResponse = response.body();
                Log.d("DEBUG:ABRIR CONEXAO API", apiResponse.getStatus());
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                Log.d("ERRO ABRIR CONEXAO API", t.getMessage());
            }
        });
    }

    public static void sleep(long timeToWait){
        try {
            Thread.sleep(timeToWait);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String stringTwoDecimalPlaces(BigDecimal bigDecimal){
        //return new DecimalFormat("#0.##").format(bigDecimal);
        return String.format("%,.2f", bigDecimal.setScale(2, RoundingMode.DOWN));
    }
}
